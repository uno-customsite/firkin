<?php
	//set_time_limit(60);
	include("./includes/client.php");
	
	$request = array(
        'id' => CLIENT_API_ID,
        'api_secret' => CLIENT_API_KEY,
        'request' => array(
            'name' => "getWebProfileInfo",
			'arguments' => array(
					'profile' => PROFILE
				)	
            ),
     );
	$res = curl_post(API_LINK, array("request"=>json_encode($request)));
	
	$res=utf8_encode($res);
		
	$prfarr = json_decode($res,  $assoc = true);
	/*print_r($prfarr);
	exit();*/
			
	@$pagename=$_GET["pg"];
	if(strpos($pagename,"/")!==false)
	{
		if(strpos($pagename,"/")==strlen($pagename)-1) $pagename=substr($pagename,0,strlen($pagename)-1);  
	}	
	
	$imagepath="webdata/".strtolower(PROFILEID)."/";	
	$contact_address=$prfarr["weblocation"].", ".$prfarr["webcity"].", ".$prfarr["webstate"].", ".$prfarr["webcountry"].", ".$prfarr["webpostalcode"];
	$contenttop=$prfarr["websitenvtop"];
	
	switch($prfarr["templateid_fk"])
	{
		case 2: 
				$csslink="webcss/variable_fontstyle.css";
				break;
		default: 
				$csslink="webcss/fixed_fontstyle.css";	
				break;
	}
	
	$main_menus = array();
	$sub_menus = array();
	$homepage_button = array();
	$btns=array();
	$redrlink=array();
	$spchar=array(" ","/","_");
	foreach($prfarr['website_buttons'] as $menu) {
		$btns[$menu['menu_id']]=str_replace($spchar,"_",$menu['menu_name']);
		//echo $menu['menu_name']." ".urlencode($menu['menu_name'])."<br>";
		if($menu["function"]=="RedirectLink") $redrlink[$menu['menu_id']]=$menu["content"];
		
		if($menu['parent_id']==0) {
			$main_menus[] = $menu;
			if($menu['is_homepage'] == 1 && count($homepage_button)==0) {
				$homepage_button = $menu;
			}
		} else {
			$sub_menus[] = $menu;
		}
			//echo strtoupper(str_replace($spchar,"",$menu['menu_name']))." ".strtoupper(str_replace($spchar,"",$pagename))."<br>";
			//if(strtoupper($menu['menu_name'])==stripslashes(strtoupper(str_replace($spchar," ",$pagename)))) 
			if(strtoupper(str_replace($spchar," ",$menu['menu_name']))==stripslashes(strtoupper(str_replace($spchar," ",$pagename)))) 
			{
				$homepage_button = $menu;
			}	
	} // for loop end here
	$main_menus=array_sort($main_menus, 'menu_order', SORT_ASC);
	$sub_menus=array_sort($sub_menus, 'menu_order', SORT_ASC);
	
	//print_r($sub_menus);
	//exit();
	
	$rediectlink=array("RedirectLink","showOpenTableReservation");
	
	if($prfarr["hasshortlink"]!=1)
	{
		$pglink="http://".$_SERVER['HTTP_HOST']."/".HOST_FOLDER.substr($_SERVER['SCRIPT_NAME'],strrpos($_SERVER['SCRIPT_NAME'],"/")+1)."?pg=";
     }
	 else
	 {
	 	$pglink="http://".$_SERVER['HTTP_HOST']."/".HOST_FOLDER;
	 }  
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<link rel="icon"http://firkinpubs.com/new/favicon.ico" type="image/x-icon"/>
<link rel="shortcut icon" href="http://firkinpubs.com/new/favicon.ico"/>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="Content-Language" content="EN">
<title><?php ?><?php echo ($prfarr["webtitle"]!="")?$prfarr["webtitle"]:PROFILE; ?><?php ?></title>

<meta name="title" content="<?php echo ($prfarr["webtitle"]!="")?$prfarr["webtitle"]:PROFILE; ?>" />
<meta name="keywords" content="<?php echo ($prfarr["webkeyword"]!="")?$prfarr["webkeyword"]:PROFILE; ?>" />
<meta name="description" content="<?php echo ($prfarr["webdescription"]!="")?$prfarr["webdescription"]:PROFILE; ?>" />
<META name="expires" content="never">
<META name="charset" content="UTF-8">
<META NAME="AUTHOR" CONTENT="Digital Menubox">
<META NAME="COPYRIGHT" CONTENT="Copyright@2010 <?php echo PROFILE; ?>">
<META NAME="DISTRIBUTION" CONTENT="Global">
<META NAME="RATING" CONTENT="General">
<META NAME="ROBOTS" CONTENT="Index, Follow">
<?
 if(trim($prfarr["analyticaccount"])!="") { ?>
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', '<? echo $prfarr["analyticaccount"] ?>']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
  
  
</script>
<? }
 ?>
<base href="<?php echo WEB_DATA_URL; ?>">
<? 
     
	
	echo getExtCssString($prfarr["websitefonttype"]);
	echo getExtCssString($prfarr["websitetopnvfonttype"]);
	echo getExtCssString($prfarr["websitesubnvfonttype"]);
 ?>

<link rel="stylesheet" href="<?php echo $csslink; ?>" />
<script language="javascript" src="includes/webjavascript.js"></script>
<script language="javascript" src="js/jquery-1.4.2.min.js"></script>
<script language="javascript" src="./js/galleria.js"></script>
<script language="javascript" src="./js/galleria.classic.js"></script>
<script language="javascript" src="js/thickbox.js"></script>
<script type="text/javascript" src="js/jquery.form.js"></script>
<style type="text/css">
body {
	<? if($prfarr["webpgbgtype"]=="Color" && $prfarr["webpgbgvalue"]!="") { ?>
	background-color: <? echo $prfarr["webpgbgvalue"]; ?>;
	<? } ?>
	<? if($prfarr["webpgbgtype"]=="Image" && $prfarr["webpgbgvalue"]!="" && fileexists($imagepath.$prfarr["webpgbgvalue"])) { ?>
	background-image:url(<? echo $imagepath.$prfarr["webpgbgvalue"]; ?>);
	<? } ?>
	<? if($prfarr["websitefonttype"]!="") { ?> 
	font-family: <? echo $prfarr["websitefonttype"]; ?>;
	<? } ?>
	<? if($prfarr["websitefontsize"]!="") { ?>
	font-size: <? echo $prfarr["websitefontsize"]; ?>px;
	<? } ?>
	<? if($prfarr["websitefontcolor"]!="") { ?>	
	color: <? echo $prfarr["websitefontcolor"]; ?>;
	<? } ?>
	
	<? if(strpos($prfarr["websitefontstyle"],"Bold")!==false) { ?>
	font-weight:bold;
	<? } else { ?>
	font-weight:normal;
	<? } ?>
	
	 <? if(strpos($prfarr["websitefontstyle"],"Italic")!==false) { ?>
	font-style:italic;
	<? } else { ?>
	font-style:normal;
	<? } ?>
	
	<? if(strpos($prfarr["websitefontstyle"],"Underline")!==false) { ?>
	text-decoration:underline;
	<? } else { ?>
	text-decoration:none;
	<? } ?>
}
a:link, a:visited, a:active {
	<? if($prfarr["websitefontcolor"]!="") { ?>
	color: <? echo $prfarr["websitefontcolor"]; ?>;
	<? } ?>
	text-decoration: none;
}
a:hover {
	<? if($prfarr["websitefontcolor"]!="") { ?>
	color: <? echo $prfarr["websitefontcolor"]; ?>;
	<? } ?>
	text-decoration: underline;	
}
.inputtext, .button
{
	<? if($prfarr["websitefonttype"]!="") { ?> 
	font-family: <? echo $prfarr["websitefonttype"]; ?>;
	<? } ?>
	<? if($prfarr["websitefontsize"]!="") { ?>
	font-size: <? echo $prfarr["websitefontsize"]; ?>px;
	<? } ?>
	<? if($prfarr["websitefontcolor"]!="") { ?>	
	color: #000000; 
	<? } ?>
}
.website
{
	<? if($prfarr["websitebgtype"]=="Color" && $prfarr["websitebgvalue"]!="") { ?>
	background-color: <? echo $prfarr["websitebgvalue"]; ?>;
	<? } ?>
	<? if($prfarr["websitebgtype"]=="Image" && $prfarr["websitebgvalue"]!="" && fileexists($imagepath.$prfarr["websitebgvalue"])) { ?>
	background-image:url(<? echo $imagepath.$prfarr["websitebgvalue"]; ?>);
	<? } ?>
}
.web_header
{
	<? if($prfarr["websitehpbgtype"]=="Color" && $prfarr["websitehpbgvalue"]!="") { ?>
	background-color: <? echo $prfarr["websitehpbgvalue"]; ?>;
	<? } ?>
	<? if($prfarr["websitehpbgtype"]=="Image" && $prfarr["websitehpbgvalue"]!="" && fileexists($imagepath.$prfarr["webpgbgvalue"])) { ?>
	background-image:url(<? echo $imagepath.$prfarr["websitehpbgvalue"]; ?>);
	background-repeat:no-repeat;
	<? } ?>
	<? if($prfarr["websitehpheight"]!=0 || 1==1) {?>
	height: <? echo $prfarr["websitehpheight"]; ?>px;
	<? } ?>
}
.footer
{
	<? if($prfarr["websitefpbgtype"]=="Color" && $prfarr["websitefpbgvalue"]!="") { ?>
	background-color: <? echo $prfarr["websitefpbgvalue"]; ?>;
	<? } ?>
	<? if($prfarr["websitefpbgtype"]=="Image" && $prfarr["websitefpbgvalue"]!="" && fileexists($imagepath.$prfarr["websitefpbgvalue"])) { ?>
	background-image:url(<? echo $imagepath.$prfarr["websitefpbgvalue"]; ?>);
	<? } ?>
	<? if($prfarr["websitefpbgtype"]=="Color" && $prfarr["websitefpbgvalue"]!="") { ?>
	background-color: <? echo $prfarr["websitefpbgvalue"]; ?>;
	<? } ?>
	<? if($prfarr["websitefpbgtype"]=="Image" && $prfarr["websitefpbgvalue"]!="" && fileexists($imagepath.$prfarr["websitefpbgvalue"])) { ?>
	background-image:url(<? echo $imagepath.$prfarr["websitefpbgvalue"]; ?>);
	<? } ?>
	<? if($prfarr["websitefpfontsize"]!="") { ?> 
	font-size: <? echo $prfarr["websitefpfontsize"]; ?>px;
	<? } ?>
	<? if($prfarr["websitefpfontcolor"]!="") { ?> 
	color: <? echo $prfarr["websitefpfontcolor"]; ?>;
	<? } ?>
	
	<? if(strpos($prfarr["websitefpfontstyle"],"Bold")!==false) { ?>
	font-weight:bold;
	<? } else { ?>
	font-weight:normal;
	<? } ?>
	
	 <? if(strpos($prfarr["websitefpfontstyle"],"Italic")!==false) { ?>
	font-style:italic;
	<? } 
	else { ?>
	font-style:normal;
	<? } ?>
	
	<? if(strpos($prfarr["websitefpfontstyle"],"Underline")!==false) { ?>
	text-decoration:underline;
	<? } else { ?>
	text-decoration:none;
	<? } ?>
}
.footer a:link, .footer a:visited, .footer a:active {
	<? if($prfarr["websitefpfontsize"]!="") { ?> 
	font-size: <? echo $prfarr["websitefpfontsize"]; ?>px;
	<? } ?>
	<? if($prfarr["websitefpfontcolor"]!="") { ?> 
	color: <? echo $prfarr["websitefpfontcolor"]; ?>;
	<? } ?>
	
	<? if(strpos($prfarr["websitefpfontstyle"],"Bold")!==false) { ?>
	font-weight:bold;
	<? } else { ?>
	font-weight:normal;
	<? } ?>
	
	 <? if(strpos($prfarr["websitefpfontstyle"],"Italic")!==false) { ?>
	font-style:italic;
	<? }else { ?>
	font-style:normal;
	<? } ?>
	
	<? if(strpos($prfarr["websitefpfontstyle"],"Underline")!==false) { ?>
	text-decoration:underline;
	<? } else { ?>
	text-decoration:none;
	<? } ?>
}
.footer a:hover {
	<? if($prfarr["websitefpfontsize"]!="") { ?> 
	font-size: <? echo $prfarr["websitefpfontsize"]; ?>px;
	<? } ?>
	<? if($prfarr["websitefprfontcolor"]!="") { ?> 
	color: <? echo $prfarr["websitefprfontcolor"]; ?>;
	<? } ?>
	
	<? if(strpos($prfarr["websitefprfontstyle"],"Bold")!==false) { ?>
	font-weight:bold;
	<? } else { ?>
	font-weight:normal;
	<? } ?>
	
	 <? if(strpos($prfarr["websitefprfontstyle"],"Italic")!==false) { ?>
	font-style:italic;
	<? }else { ?>
	font-style:normal;
	<? } ?>
	
	<? if(strpos($prfarr["websitefprfontstyle"],"Underline")!==false) { ?>
	text-decoration:underline;
	<? } else { ?>
	text-decoration:none;
	<? } ?>
}
.footer
{
	position:relative;	
}
.leftpanel
{
	
	<? if($prfarr["websitelpbgtype"]=="Color" && $prfarr["websitelpbgvalue"]!="") { ?>
	background-color: <? echo $prfarr["websitelpbgvalue"]; ?>;
	<? } ?>
	<? if($prfarr["websitelpbgtype"]=="Image" && $prfarr["websitelpbgvalue"]!="" && fileexists($imagepath.$prfarr["websitelpbgvalue"])) { ?>
	background-image:url(<? echo $imagepath.$prfarr["websitelpbgvalue"]; ?>);
	<? } ?>
	width:<? echo $prfarr["websitelpwidth"]; ?>px;
	<? if($prfarr["websitelpwidth"]==0) { ?>
		display:none;
	<? } ?>
}
.rightpanel
{
	<? if($prfarr["websiterpbgtype"]=="Color" && $prfarr["websiterpbgvalue"]!="") { ?>
	background-color: <? echo $prfarr["websiterpbgvalue"]; ?>;
	<? } ?>
	<? if($prfarr["websiterpbgtype"]=="Image" && $prfarr["websiterpbgvalue"]!="" && fileexists($imagepath.$prfarr["websiterpbgvalue"])) { ?>
	background-image:url(<? echo $imagepath.$prfarr["websiterpbgvalue"]; ?>);
	<? } ?>
	width:<? echo $prfarr["websiterpwidth"]; ?>px;
	<? if($prfarr["websiterpwidth"]==0) { ?>
		display:none;
	<? } ?>
}

.social_media
{
	margin-left:10px;
	top:<? echo ($prfarr["icon_position"]=="left_top")?$contenttop:675; ?>px; 
	position:absolute; 	
}
.social_media img
{
	padding-right:5px;
	border:0px;
}
.overlay
{
	display: none;
	position: absolute;
	top: 0%;
	left: 0%;
	width: 100%;
	height: 100%;
	background-color: black;
	z-index:0.5;
	-moz-opacity: 0.5;
	opacity:.50;
	filter: alpha(opacity=50);
}
.popupapi
{
	display: none;
	position: absolute;
	top:5%;
	left:30%;
	z-index:200;
}
.menu, .menu:active, .menu:link, .menu:visited
{
	<? if($prfarr["websitenvbgvalue"]!="" && fileexists($imagepath.$prfarr["websitenvbgvalue"])) { ?>
	background-image:url(<? echo $imagepath.$prfarr["websitenvbgvalue"]; ?>);
	<? } ?>
    <? if($prfarr["websitenvfontalign"]!="") { ?> 
	text-align:<? echo $prfarr["websitenvfontalign"]; ?>;
	<? } ?>

	<? if($prfarr["websitenvfonttype"]!="") { ?> 
	font-family: <? echo $prfarr["websitenvfonttype"]; ?>;
	<? } ?>
	<? if($prfarr["websitenvfontsize"]!="") { ?> 
	font-size: <? echo $prfarr["websitenvfontsize"]; ?>px;
	<? } ?>
	<? if($prfarr["websitenvfontcolor"]!="") { ?> 
	color: <? echo $prfarr["websitenvfontcolor"]; ?>;
	<? } ?>
	
	<? if(strpos($prfarr["websitenvfontstyle"],"Bold")!==false) { ?>
	font-weight:bold;
	<? } else { ?>
	font-weight:normal;
	<? } ?>
	
	 <? if(strpos($prfarr["websitenvfontstyle"],"Italic")!==false) { ?>
	font-style:italic;
	<? } else { ?>
	font-style:normal;
	<? } ?>
	
	<? if(strpos($prfarr["websitenvfontstyle"],"Underline")!==false) { ?>
	text-decoration:underline;
	<? } else { ?>
	text-decoration:none;
	<? } ?>
	height:<? echo $prfarr["websitenvheight"]; ?>px;
    <?php if($prfarr["websitenvfontalign"]=="left") { ?>
    text-indent:20px;
    <? } ?>
    cursor: pointer;
}
.menuselected
{
    <? if($prfarr["websitenvfontalign"]!="") { ?> 
	text-align:<? echo $prfarr["websitenvfontalign"]; ?>;
	<? } ?>
	<? if($prfarr["websitenvrbgvalue"]!="" && fileexists($imagepath.$prfarr["websitenvrbgvalue"])) { ?>
	background-image:url(<? echo $imagepath.$prfarr["websitenvrbgvalue"]; ?>);
	<? } ?>
	<? if($prfarr["websitenvfonttype"]!="") { ?> 
	font-family: <? echo $prfarr["websitenvfonttype"]; ?>;
	<? } ?>
	<? if($prfarr["websitenvfontsize"]!="") { ?> 
	font-size: <? echo $prfarr["websitenvfontsize"]; ?>px;
	<? } ?>
	<? if($prfarr["websitenvrfontcolor"]!="") { ?> 
	color: <? echo $prfarr["websitenvrfontcolor"]; ?>;
	<? } ?>
	
	<? if(strpos($prfarr["websitenvrfontstyle"],"Bold")!==false) { ?>
	font-weight:bold;
	<? } else { ?>
	font-weight:normal;
	<? } ?>
	
	 <? if(strpos($prfarr["websitenvrfontstyle"],"Italic")!==false) { ?>
	font-style:italic;
	<? } else { ?>
	font-style:normal;
	<? } ?>
	
	<? if(strpos($prfarr["websitenvrfontstyle"],"Underline")!==false) { ?>
	text-decoration:underline;
	<? } else { ?>
	text-decoration:none;
	<? } ?>
	height:<? echo $prfarr["websitenvheight"]; ?>px;
    <?php if($prfarr["websitenvfontalign"]=="left") { ?>
    text-indent:20px;
    <? } ?>
}
.topbutton
{
	height:<? echo $prfarr["websitetopheight"]; ?>px;
	<? if($prfarr["websitetopbgvalue"]!="" && fileexists($imagepath.$prfarr["websitetopbgvalue"])) { ?>
	background-image:url(<? echo $imagepath.$prfarr["websitetopbgvalue"]; ?>);
	<? } ?>
	overflow:hidden;
}
.topmenu, .topmenu:active, .topmenu:link, .topmenu:visited
{
	width:<? echo $prfarr["websitetopnvwidth"]; ?>px;
	height:<? echo $prfarr["websitetopheight"]; ?>px;
	<? if($prfarr["websitetopnvbgvalue"]!="" && fileexists($imagepath.$prfarr["websitetopnvbgvalue"])) { ?>
	background-image:url(<? echo $imagepath.$prfarr["websitetopnvbgvalue"]; ?>);
	<? } ?>
    <? if($prfarr["websitetopnvfontalign"]!="") { ?> 
	text-align:<? echo $prfarr["websitetopnvfontalign"]; ?>;
	<? } ?>
	<? if($prfarr["websitetopnvfonttype"]!="") { ?> 
	font-family: <? echo $prfarr["websitetopnvfonttype"]; ?>;
	<? } ?>
	<? if($prfarr["websitetopnvfontsize"]!="") { ?> 
	font-size: <? echo $prfarr["websitetopnvfontsize"]; ?>px;
	<? } ?>
	<? if($prfarr["websitetopnvfontcolor"]!="") { ?> 
	color: <? echo $prfarr["websitetopnvfontcolor"]; ?>;
	<? } ?>
	
	<? if(strpos($prfarr["websitetopnvfontstyle"],"Bold")!==false) { ?>
	font-weight:bold;
	<? } else { ?>
	font-weight:normal;
	<? } ?>
	
	 <? if(strpos($prfarr["websitetopnvfontstyle"],"Italic")!==false) { ?>
	font-style:italic;
	<? } else { ?>
	font-style:normal;
	<? } ?>
	
	<? if(strpos($prfarr["websitetopnvfontstyle"],"Underline")!==false) { ?>
	text-decoration:underline;
	<? } else { ?>
	text-decoration:none;
	<? } ?>
	text-align:<? echo $prfarr["websitenvfontalign"]; ?>
    cursor: pointer;
}
.topmenu:active, .topmenu:link, .topmenu:visited
{
	height:auto;
}
.topmenuselected
{
	width:<? echo $prfarr["websitetopnvwidth"]; ?>px;
	height:<? echo $prfarr["websitetopheight"]; ?>px;
    <? if($prfarr["websitetopnvfontalign"]!="") { ?> 
	text-align:<? echo $prfarr["websitetopnvfontalign"]; ?>;
	<? } ?>
	<? if($prfarr["websitetopnvrbgvalue"]!="" && fileexists($imagepath.$prfarr["websitetopnvrbgvalue"])) { ?>
	background-image:url(<? echo $imagepath.$prfarr["websitetopnvrbgvalue"]; ?>);
	<? } ?>
	<? if($prfarr["websitetopnvfonttype"]!="") { ?> 
	font-family: <? echo $prfarr["websitetopnvfonttype"]; ?>;
	<? } ?>
	<? if($prfarr["websitetopnvfontsize"]!="") { ?> 
	font-size: <? echo $prfarr["websitetopnvfontsize"]; ?>px;
	<? } ?>
	<? if($prfarr["websitetopnvrfontcolor"]!="") { ?> 
	color: <? echo $prfarr["websitetopnvrfontcolor"]; ?>;
	<? } ?>
	
	<? if(strpos($prfarr["websitetopnvrfontstyle"],"Bold")!==false) { ?>
	font-weight:bold;
	<? } else { ?>
	font-weight:normal;
	<? } ?>
	
	 <? if(strpos($prfarr["websitetopnvrfontstyle"],"Italic")!==false) { ?>
	font-style:italic;
	<? } else { ?>
	font-style:normal;
	<? } ?>
	
	<? if(strpos($prfarr["websitetopnvrfontstyle"],"Underline")!==false) { ?>
	text-decoration:underline;
	<? } else { ?>
	text-decoration:none;
	<? } ?>
	
}
.address
{
    <? if($prfarr["websitenvfontalign"]!="") { ?> 
        text-align:<? echo $prfarr["websitenvfontalign"]; ?>;
	<? } ?>
    height:22px;
	padding-left:30px;
}
/* .menulink:active, .menulink:link, .menulink:visited, .menulink  
{
	<? if($prfarr["websitenvfonttype"]!="") { ?> 
	font-family: <? echo $prfarr["websitenvfonttype"]; ?>;
	<? } ?>
	<? if($prfarr["websitenvfontsize"]!="") { ?> 
	font-size: <? echo $prfarr["websitenvfontsize"]; ?>;
	<? } ?>
	<? if($prfarr["websitenvfontcolor"]!="") { ?> 
	color: <? echo $prfarr["websitenvfontcolor"]; ?>;
	<? } ?>
	<? if(strpos($prfarr["websitenvfontstyle"],"Bold")!==false) { ?>
	font-weight:bold;
	<? } ?>
	 <? if(strpos($prfarr["websitenvfontstyle"],"Italic")!==false) { ?>
	font-style:italic;
	<? } ?>
	<? if(strpos($prfarr["websitenvfontstyle"],"Underline")!==false) { ?>
	text-decoration:underline;
	<? } else { ?>
	text-decoration:none;
	<? } ?>
	text-align:left;
}
.menulink:hover
{
	<? if($prfarr["websitenvfonttype"]!="") { ?> 
	font-family: <? echo $prfarr["websitenvfonttype"]; ?>;
	<? } ?>
	<? if($prfarr["websitenvfontsize"]!="") { ?> 
	font-size: <? echo $prfarr["websitenvfontsize"]; ?>;
	<? } ?>
	<? if($prfarr["websitenvrfontcolor"]!="") { ?> 
	color: <? echo $prfarr["websitenvrfontcolor"]; ?>;
	<? } ?>
	<? if(strpos($prfarr["websitenvrfontstyle"],"Bold")!==false) { ?>
	font-weight:bold;
	<? } ?>
	 <? if(strpos($prfarr["websitenvrfontstyle"],"Italic")!==false) { ?>
	font-style:italic;
	<? } ?>
	<? if(strpos($prfarr["websitenvrfontstyle"],"Underline")!==false) { ?>
	text-decoration:underline;
	<? } else { ?>
	text-decoration:none;
	<? } ?>
	text-align:left;
} */

.submenu {
    border-collapse: collapse;
    cursor: pointer;
    display:none;
}

.submenu_item, .submenu_item a:link, .submenu_item a:active, .submenu_item a:visited{
    <? if($prfarr["websitesubnvfontalign"]!="") { ?> 
	text-align:<? echo $prfarr["websitesubnvfontalign"]; ?>;
	<? } ?>
 	<? if($prfarr["websitesubnvbgvalue"]!="" && fileexists($imagepath.$prfarr["websitesubnvbgvalue"])) { ?>
	background-image:url(<? echo $imagepath.$prfarr["websitesubnvbgvalue"]; ?>);
	<? } ?>
	<? if($prfarr["websitesubnvfonttype"]!="") { ?> 
	font-family: <? echo $prfarr["websitesubnvfonttype"]; ?>;
	<? } ?>
	<? if($prfarr["websitesubnvfontsize"]!="") { ?> 
	font-size: <? echo $prfarr["websitesubnvfontsize"]; ?>px;
	<? } ?>
	<? if($prfarr["websitesubnvfontcolor"]!="") { ?> 
	color: <? echo $prfarr["websitesubnvfontcolor"]; ?>;
	<? } ?>
	<? if(strpos($prfarr["websitesubnvfontstyle"],"Bold")!==false) { ?>
	font-weight:bold;
	<? } ?>
	 <? if(strpos($prfarr["websitesubnvfontstyle"],"Italic")!==false) { ?>
	font-style:italic;
	<? } ?>
	<? if(strpos($prfarr["websitesubnvfontstyle"],"Underline")!==false) { ?>
	text-decoration:underline;
	<? } else { ?>
	text-decoration:none;
	<? } ?>
	border-collapse: collapse;
    cursor: pointer;
    height:30px;
    line-height: 30px;
	/*margin:0 10px;*/
    <?php if($prfarr["websitesubnvfontalign"]=="left") { ?>
    text-indent:25px;
    <? } ?>
}
.submenu_item_selected , .submenu_item_selected a:link, .submenu_item_selected a:active, .submenu_item_selected a:visited {
	<? if($prfarr["websitesubnvrbgvalue"]!="" && fileexists($imagepath.$prfarr["websitesubnvrbgvalue"])) { ?>
	background-image:url(<? echo $imagepath.$prfarr["websitesubnvrbgvalue"]; ?>);
	<? } ?>
	<? if($prfarr["websitesubnvrfonttype"]!="") { ?> 
	font-family: <? echo $prfarr["websitesubnvfonttype"]; ?>;
	<? } ?>
	<? if($prfarr["websitesubnvrfontsize"]!="") { ?> 
	font-size: <? echo $prfarr["websitesubnvrfontsize"]; ?>px;
	<? } ?>
	<? if($prfarr["websitesubnvrfontcolor"]!="") { ?> 
	color: <? echo $prfarr["websitesubnvrfontcolor"]; ?>;
	<? } ?>
	<? if(strpos($prfarr["websitesubnvrfontstyle"],"Bold")!==false) { ?>
	font-weight:bold;
	<? } ?>
	 <? if(strpos($prfarr["websitesubnvrfontstyle"],"Italic")!==false) { ?>
	font-style:italic;
	<? } ?>
	<? if(strpos($prfarr["websitesubnvrfontstyle"],"Underline")!==false) { ?>
	text-decoration:underline;
	<? } else { ?>
	text-decoration:none;
	<? } ?>
    <?php if($prfarr["websitesubnvfontalign"]=="left") { ?>
    text-indent:25px;
    <? } ?>
    border-collapse: collapse;
    cursor: pointer;
}
.submenu_item:hover, .submenu_item:hover a:link, .submenu_item:hover a:active, .submenu_item:hover a:visited {
	<? if($prfarr["websitesubnvrbgvalue"]!="" && fileexists($imagepath.$prfarr["websitesubnvrbgvalue"])) { ?>
	background-image:url(<? echo $imagepath.$prfarr["websitesubnvrbgvalue"]; ?>);
	<? } ?>
	<? if($prfarr["websitesubnvrfonttype"]!="") { ?> 
	font-family: <? echo $prfarr["websitesubnvfonttype"]; ?>;
	<? } ?>
	<? if($prfarr["websitesubnvrfontsize"]!="") { ?> 
	font-size: <? echo $prfarr["websitesubnvrfontsize"]; ?>px;
	<? } ?>
	<? if($prfarr["websitesubnvrfontcolor"]!="") { ?> 
	color: <? echo $prfarr["websitesubnvrfontcolor"]; ?>;
	<? } ?>
	<? if(strpos($prfarr["websitesubnvrfontstyle"],"Bold")!==false) { ?>
	font-weight:bold;
	<? } ?>
	 <? if(strpos($prfarr["websitesubnvrfontstyle"],"Italic")!==false) { ?>
	font-style:italic;
	<? } ?>
	<? if(strpos($prfarr["websitesubnvrfontstyle"],"Underline")!==false) { ?>
	text-decoration:underline;
	<? } else { ?>
	text-decoration:none;
	<? } ?>
    border-collapse: collapse;
    cursor: pointer;
}
#webgallery
{
	height: <? echo $prfarr["websitegalleryheight"]; ?>px;
	overflow:hidden;
}
.yang_gallery {
    position: relative;
}
.pagination {
    position: absolute;
    bottom:20px;
    text-align:center;
}
.pages {
    display:inline-block;
    border:1px solid #222;
    width:30px;
    height:30px;
    line-height:30px;
    margin:0 5px;
    font-weight:bold;

}
.current_page {
    background-color:#444;
    color: white;
}
.gallery_next, .gallery_prev {
	line-height:30px;
	margin: 5px 10px;
	cursor: pointer;
}
.action_button {
	cursor: pointer;
}
/*****************/
.spacer
{
	height:<? echo $prfarr["websitenvseparator"]; ?>px;
}
.topmenuspacer
{
	height:<? echo $prfarr["websitetopnvseperatorwidth"]; ?>px;
}
.errmsg
{
	color:#FF0000;
}

#slideshow {
    position:relative;
    height:580px;
	}

#slideshow IMG {
	vertical-align:middle;
    position:absolute;
	top:0px;
	left:5px;
    z-index:8;
    opacity:0.0;
	visibility:hidden;
}

#slideshow IMG.active {
	z-index:10;
    opacity:1.0;
	visibility:visible;
}

#slideshow IMG.last-active {
	z-index:9;
	visibility:hidden;
}
 #galleria{height:570px;width:<?=(1024-20-$prfarr['websitelpwidth']-$prfarr['websitelpwidth'])?>px;margin:0px auto;}
.music
{	
	width:<?=$prfarr['musicwidth']?>px;
	position:absolute;
	border:0px solid <?=$prfarr['musicfontcolor']?> ;
	background-color:<? if ($prfarr['musictransparent']=="yes") echo "transparent;";
else echo $prfarr['musicbgcolor']?>; 
	<?
		$musicmargin_center=(int)((1024-$prfarr['musicwidth'])/2);
		$musicmargin_right=(int)(1024-$prfarr['musicwidth']);
		if ($prfarr['musicalign']=='left') 		echo '';
		else if ($prfarr['musicalign']=='center') 	echo 'margin-left:'.$musicmargin_center.'px;'; 
		else if ($prfarr['musicalign']=='right') 	echo 'margin-left:'.$musicmargin_right.'px;'; 
	?>
	
}
.dmb_logo {
    height: 45px;
    position: absolute;
    right: 5px;
}
 </style>
<script language="javascript" src="includes/CalendarControl.js"></script>
<link type='text/css' rel='stylesheet' href='includes/CalendarControl.css' />
<link rel="stylesheet" href="js/thickbox.css" />
<script language="javascript">
var pause = 10000;
var galleryRotateTimer = 0;
var btnstr= {"bindings": [<? echo json_encode($btns); ?>]};
var redlink = {"bindings": [<? echo json_encode($redrlink); ?>]};
function showSubmenu(menuid) {
    submenu = $("#submenu"+menuid);
   	$(".submenu").each(function() {
        $(this).hide();
    });
    $(submenu).show();
}
$(document).ready(function() {
	var hbutton="#button" + "<?=$homepage_button["menu_id"]?>";
	if($(hbutton).length>0)
	{
			if($(hbutton).hasClass("menu"))
			{
				$(hbutton).removeClass("menu");
				$(hbutton).addClass("menuselected");
			}	
			if($(hbutton).hasClass("topmenu"))
			{
				$(hbutton).removeClass("topmenu");
				$(hbutton).addClass("topmenuselected");
			}	
	}
	
	
	/*var parentid="<?$homepage_button["parent_id"]?>";
	if(parentid!="0")
	{
		$("#button" + parentid).trigger('click');
		$(hbutton).addClass("submenu_item_selected");
	}*/
	//new code start to highlight subbutton
	var parentid="<?=$homepage_button["parent_id"]?>";
	if(parentid!="0")
	{
		$(hbutton).addClass("submenu_item_selected");
		
		var parent_button=$("#button" + parentid);
		parent_button.trigger('click');
		
		/* -- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---  -- */	
		/* -- this is to highlight parent button as well when children is selected -- */
			if(parent_button.hasClass("menu"))
			{
				parent_button.removeClass("menu");
				parent_button.addClass("menuselected");
			}	
			if(parent_button.hasClass("topmenu"))
			{
				parent_button.removeClass("topmenu");
				parent_button.addClass("topmenuselected");
			}
		/* -- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---  -- */	
	}
	
	/* -- this is to check and then show submenu when parent has submenu -- */
		else if ($("#submenu<?=$homepage_button["menu_id"]?>").length>0)
		{
			$("#submenu<?=$homepage_button["menu_id"]?>").show();	
		}
		//new code end
	/* -- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---  -- */	
	
	
	$('.action_button').click( function(){
		//alert($(this).attr('function'));
		
		//pgnm=$(this).html();
		
		pgid=$(this).attr('buttonid');
		pgnm=btnstr.bindings[0][pgid];
		gallerynm=$(this).attr('pagenumber');
		func=$(this).attr('function');
		
		//+ '&pgid=' + pgid
		
		/* pglink='<? echo "http://".$_SERVER['HTTP_HOST']."/".HOST_FOLDER.substr($_SERVER['SCRIPT_NAME'],strrpos($_SERVER['SCRIPT_NAME'],"/")+1)?>?pg=' + pgnm.toLowerCase(); */
		pglinkurl='<? echo $pglink ?>' +encodeURIComponent(encodeURIComponent(pgnm.toLowerCase()));
		
		if(typeof(gallerynm)!='undefined')  pglinkurl=pglinkurl + "&page=" + gallerynm;
		if(func!="RedirectLink")
		{
		 	go_url(pglinkurl);
		 }
		 else
		 {
		 	top.location=redlink.bindings[0][pgid];
		 }
		
	});
	/* $(".pagination .action_button").live('click', function(){
		pglink='<? echo "http://".$_SERVER['HTTP_HOST']."/".HOST_FOLDER.substr($_SERVER['SCRIPT_NAME'],strrpos($_SERVER['SCRIPT_NAME'],"/")+1)?>?pg=<?=$pagename?>&pgid=<?=$_GET["pgid"]?>' ;
		alert(pglink);
		go_url(pglink);
		return;
	}); */
		
		$(".menu").click(function() {
        $(".submenu").each(function() {
            $(this).hide();
        });
        var menuId = $(this).attr("id");
        menuId = menuId.replace("button", "");
        $("#submenu"+menuId).show();

        if($(this).is('.unclickable')) {
            return;
        }
        
		$(".submenu_item_selected").each(function() {
            $(this).removeClass("submenu_item_selected");
         });
         $(".menuselected").each(function() {
             	if(!$(this).hasClass("nochange")) {
				$(this).removeClass("menuselected");
            	$(this).addClass("menu");
				}
		   }); 
		
		$(this).removeClass("menu");
        $(this).addClass("menuselected");
		
		$(".topmenuselected").each(function() {
             	if(!$(this).hasClass("nochange")) {
				$(this).removeClass("topmenuselected");
            	$(this).addClass("topmenu");
				}
		   });
		   
	});
	 
	 $(".topmenu").click(function() {
	 		$(".submenu").each(function() {
            $(this).hide();
        	});
			var menuId = $(this).attr("id");
		    menuId = menuId.replace("button", "");
       		$("#submenu"+menuId).show();
			$(".submenu_item_selected").each(function() {
            $(this).removeClass("submenu_item_selected");
        	 });
			 
			$(".menuselected").each(function() {
				$(this).removeClass("menuselected");
				$(this).addClass("menu");
			});
			
			$(".topmenuselected").each(function() {
             	if(!$(this).hasClass("nochange")) {
				$(this).removeClass("topmenuselected");
            	$(this).addClass("topmenu");
				}
		   });
		  $(this).removeClass("topmenu");
          $(this).addClass("topmenuselected");
	 });
	

	$(".submenu_item").click(function() {
        if($(this).is('.unclickable')) {
            return;
        }
	   $(".submenu_item_selected").each(function() {
			$(this).removeClass("submenu_item_selected");
		});
	   $(this).addClass("submenu_item_selected");
	   resetFocus();
   });
    $('.gallery_next').click(function() {
		nextImage();
		clearTimeout(galleryRotateTimer);
		galleryRotateTimer = setTimeout("rotateGallery(pause)", pause);
   });
   
   $('.gallery_prev').click(function() {
		prevImage();
		clearTimeout(galleryRotateTimer);
		galleryRotateTimer = setTimeout("rotateGallery(pause)", pause);
   });
   $(".web_header").click(function() {
		pglink="<? echo "http://".$_SERVER['HTTP_HOST']."/".HOST_FOLDER.substr($_SERVER['SCRIPT_NAME'],strrpos($_SERVER['SCRIPT_NAME'],"/")+1)?>";
		go_url(pglink);
	});
   
   
});
function resetFocus() {
	$("#focus").hide(); 
}
function openpopup()
{
	$("#blackoverlay").show();
	$("#largeapi").show();
}
function closepopup()
{
	$("#blackoverlay").hide();
	$("#largeapi").hide();
}
function slideSwitch() {
    var $active = $('#slideshow IMG.active');

    if ( $active.length == 0 ) $active = $('#slideshow IMG:last');

    var $next =  $active.next().length ? $active.next()
        : $('#slideshow IMG:first');

    // uncomment the 3 lines below to pull the images in random order
    
    // var $sibs  = $active.siblings();
    // var rndNum = Math.floor(Math.random() * $sibs.length );
    // var $next  = $( $sibs[ rndNum ] );


    $active.addClass('last-active');

    $next.css({opacity: 0.0})
        .addClass('active')
        .animate({opacity: 1.0}, 1000, function() {
            $active.removeClass('active last-active');
        });
}
$('.imagegal a').click( function() {
	var imageid = $(this).attr("imageid");
	var galleryid = $(this).attr("galleryid");
	var cntpanel = $("#cwidth").val();
	$("#contentid").load("<?=CLIENT_URL?>website_ops.php", "datatype=scrollGallery&buttonid="+galleryid+"&language=php&version=5&width="+cntpanel,function(response, status, xhr) {
		$("#img"+imageid).show();
		clearTimeout(galleryRotateTimer);
		galleryRotateTimer = setTimeout("rotateGallery(pause)", pause);
		
	}); 
});
function rotateGallery(pause) {
	nextImage();
	galleryRotateTimer = setTimeout("rotateGallery(pause)", pause);
}
function nextImage() {
	var item = $(".scrollableGallery:visible");
	//alert(item.next().html()==null);
	if(item.next().html()==null) {
		//item.hide();
		$(".scrollableGallery:eq(0)").fadeIn(1000);
		//alert(item.first().html());
	} else {
		item.hide().next().fadeIn(1000);
	}
}

function prevImage() {
	var item = $(".scrollableGallery:visible");
	//alert(item.next().html()==null);
	if(item.prev().html()==null) {
		//item.hide();
		$(".scrollableGallery:last").fadeIn(1000);
		//alert(item.first().html());
	} else {
		item.hide().prev().fadeIn(1000);
	}
}
function resetFocus() {
	$("#focus").hide(); 
}
var loaded= false;
function lightboxgallery(btnid,lightwidth,lightheight,cnt)
{
	 		var imageno=0;
			var cntpanel = $("#cwidth").val();
			if($(this).attr('imageid')!=null) {
                imageno = $(this).attr('imageid');
            }
			
           $("#contentid").load("<?=CLIENT_URL?>website_ops.php", "datatype=lightboxFullGallery&buttonid="+btnid+"&lightboxwidth="+lightwidth+"&lightboxheight="+lightheight+"&width=" +cntpanel+"&lightboxposition="+cnt,function(response, status, xhr) {
		   		loaded=false;
				showgallery(imageno);
		   });
}
function portablelightboxgallery(btnid,lightwidth,lightheight,cnt)
{
	if (typeof(cnt)=='undefined') cnt="";
	
	 		var imageno=0;
			var cntpanel = $("#cwidth").val();
			if($(this).attr('imageid')!=null) {
                imageno = $(this).attr('imageid');
            }
			
           $("#lightboxgallery").load("<?=CLIENT_URL?>website_ops.php", "datatype=lightboxFullGallery&buttonid="+btnid+"&lightboxwidth="+lightwidth+"&lightboxheight="+lightheight+"&width=" +cntpanel+"&lightboxposition="+cnt,function(response, status, xhr) {
		   		loaded=false;
				showgallery(imageno);
		   });
}
function showgallery(selindex)
{			
			jQuery.browser = {};
			(function () {
			    jQuery.browser.msie = false;
			    jQuery.browser.version = 0;
			    if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
			        jQuery.browser.msie = true;
			        jQuery.browser.version = RegExp.$1;
			    }
			})();
			//$("#popupoverlay").show();
			var r = (jQuery.browser.msie)? "?r=" + Math.random(10000) : "";
			$("#galleria").show();
			 if(!loaded) Galleria.loadTheme('./js/galleria.classic.js' + r);
			  $('#galleria').galleria().bind(Galleria.IMAGE, function() {
				   if(!loaded) {	   	
					showselected(selindex);
				   	loaded=true;
				   } 
			   });
		}
function showselected(selind)
{
				  $(".galleria-thumbnails div").each(function() {
			  		if(selind == $(this).index()) {
						$(this).trigger("click");
					}
			  });
}
function showmap(mid)
{
	$("contentid").html("<iframe name=\"contact_map\" id=\"contact_map\" width=\"100%\" height=\"568\" frameborder=\"0\" scrolling=\"no\" marginheight=\"0\" marginwidth=\"0\" src=\"http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=<? echo $contact_address; ?>&amp;z=14&amp;output=embed\"></iframe>");
	menuclick(mid);
}
function menuclick(mid)
{
	cmenu=mid;
	var elmts=document.getElementById("maincontener").getElementsByTagName('td');
	for(i=0;i<elmts.length;i++)
	{
		if(elmts[i].className=="menu" || elmts[i].className=="menuselected")
		{
			elmts[i].className=(elmts[i].id==mid)?"menuselected":"menu";
		}
	}
}
function sendenquiry()
{
	if(trim(document.getElementById("name").value)=="")
		{
			alert("Missing : Full Name");
			document.getElementById("name").focus();
			return false;
		}
		if(trim(document.getElementById("contactno").value)=="")
		{
			alert("Missing : Contact No.");
			document.getElementById("contactno").focus();
			return false;
		}
		if(trim(document.getElementById("contactemail").value)=="")
		{
			alert("Missing : Contact Email");
			document.getElementById("contactemail").focus();
			return false;
		}
		if(validateEmail(document.getElementById("contactemail"))==false)
		{
			alert("Invalid : Contact Email");
			document.getElementById("contactemail").focus();
			return false;
		}
		$("#contentid").load("<?=CLIENT_URL?>website_ops.php", "datatype=sendenquiry&" + $("#form1").serialize(),function(response, status, xhr) {
				
	 	});
		return false;	 		
}
function sendSubscribeNewsletterForm()
{
		if(trim(document.getElementById("firstname").value)=="")
		{
			alert("Missing : Name");
			document.getElementById("firstname").focus();
			return false;
		}
		if(trim(document.getElementById("contactemail").value)=="")
		{
			alert("Missing : Contact Email");
			document.getElementById("contactemail").focus();
			return false;
		}
        if(validateEmail(document.getElementById("contactemail"))==false)
		{
			alert("Invalid : Contact Email");
			document.getElementById("contactemail").focus();
			return false;
		}
		var xmlhttp;	
		xmlhttp=createxmlhttpobject();
		if(xmlhttp == false) return false;
		$("#contentid").load("<?=CLIENT_URL?>website_ops.php", "datatype=sendSubscribeNewsletterForm&" + $("#form1").serialize(),function(response, status, xhr) {
				
	 	});
		return false;	 		
}

function sendprivateparty()
{
	if(trim(document.getElementById("fname").value)=="")
		{
			alert("Missing : First Name");
			document.getElementById("fname").focus();
			return false;
		}
		if(trim(document.getElementById("lname").value)=="")
		{
			alert("Missing : Last Name");
			document.getElementById("lname").focus();
			return false;
		}
		if(document.getElementById("bday_mm").value=="" || document.getElementById("bday_dd").value=="" || document.getElementById("bday_yy").value=="")
		{
			alert("Missing : Birthday");
			document.getElementById("bday_mm").focus();
			return false;
		}
		if(trim(document.getElementById("contactemail").value)=="")
		{
			alert("Missing : Email");
			document.getElementById("contactemail").focus();
			return false;
		}
        if(validateEmail(document.getElementById("contactemail"))==false)
		{
			alert("Invalid : Email");
			document.getElementById("contactemail").focus();
			return false;
		}
		if(trim(document.getElementById("contactno").value)=="")
		{
			alert("Missing : Phone");
			document.getElementById("contactno").focus();
			return false;
		}
		if(trim(document.getElementById("eventdate").value)=="")
		{
			alert("Missing : Event Date");
			document.getElementById("eventdate").focus();
			return false;
		}
				
		var xmlhttp;	
		xmlhttp=createxmlhttpobject();
		if(xmlhttp == false) return false;
		$("#contentid").load("<?=CLIENT_URL?>website_ops.php", "datatype=sendPrivatePartyForm&" + $("#form1").serialize(),function(response, status, xhr) {
				
	 	});
		return false;		
}


function slideSwitch() {
    var $active = $('#slideshow IMG.active');

    if ( $active.length == 0 ) $active = $('#slideshow IMG:last');

    var $next =  $active.next().length ? $active.next()
        : $('#slideshow IMG:first');

    // uncomment the 3 lines below to pull the images in random order
    
    // var $sibs  = $active.siblings();
    // var rndNum = Math.floor(Math.random() * $sibs.length );
    // var $next  = $( $sibs[ rndNum ] );


    $active.addClass('last-active');

    $next.css({opacity: 0.0})
        .addClass('active')
        .animate({opacity: 1.0}, 1000, function() {
            $active.removeClass('active last-active');
        });
}
function validateform(formname)
{
	//if(formname.required.value!=null)
	//{
		//var requiredfields=trim(formname.required.value);
		var requiredfields=trim(document.getElementById("required").value);
		if(requiredfields!="")
		{
			if(requiredfields.indexOf(",")>0) // if multiple variable
			{
				rarr=requiredfields.split(",");
				for(rc=0;rc<rarr.length;rc++)
				{
					var fcond='document.getElementById("' + rarr[rc] + '").value==""';
					if(eval(fcond)==true)
					{
						alert("Missing : " + rarr[rc]);
						return false;
					}
				}
			}
			else // if single variable
			{
				if(eval('document.getElementById("' + requiredfields + '").value==""')==true)
				{
						alert("Missing : " + requiredfields);
						return false;
				}	
			}
		}
	//}	 
	// required field not space end
	$.ajax({
		url: '<?=CLIENT_URL?>website_ops.php?datatype=otherforms',
		type: 'POST',
		data: $(formname).serialize(),
		success: function( response ) {
			//alert(stripslashes(response));
			$(formname).html(stripslashes(response));
		}	
	});
}
function validateform_with_attachment(formname)
{
	var form = $(formname);
	var requiredfields=trim(form.find("#required").val());
	if(requiredfields!="")
	{
		if(requiredfields.indexOf(",")>0) // if multiple variable
		{
			rarr=requiredfields.split(",");
			for(rc=0;rc<rarr.length;rc++)
			{
				//var fcond='document.getElementById("' + rarr[rc] + '").value==""';
				if( $.trim(form.find("#"+rarr[rc]).val()) == "")
				{
					alert("Missing : " + rarr[rc].replace("_"," "));
					return false;
				}
			}
		}
		else // if single variable
		{
			if( $.trim(form.find("#"+requiredfields).val()) == "")
			{
				alert("Missing : " + requiredfields.replace("_"," ")	);
				return false;
			}	
		}
		
	}
	$(form).ajaxSubmit({
	url:'<?=CLIENT_URL?>website_ops.php?datatype=otherforms',
	success:function(msg){ form.html(stripslashes(msg));}
	}); 
}
function stripslashes (str) {
    return (str + '').replace(/\\(.?)/g, function (s, n1) {
        switch (n1) {
        case '\\':
            return '\\';
        case '0':
            return '\u0000';
        case '':
            return '';
        default:
            return n1;
        }
    });
}

//function to resize page for ipad

 $(window).bind('orientationchange', function(event) {
    if (window.orientation == 90 || window.orientation == -90 || window.orientation == 270) {
        $('meta[name="viewport"]').attr('content', 'height=device-width,width=device-height,initial-scale=1.0,maximum-scale=1.0');
        $(window).resize();
    } else {
        $('meta[name="viewport"]').attr('content', 'height=device-height,width=device-width,initial-scale=1.0,maximum-scale=1.0');
        $(window).resize();
    }
    }).trigger('orientationchange');  

</script>
</head>
<body>
<div class="website">    	
<div class="web_header">
<?	 
	/* if(in_array($prfarr["z_webid_pk"],array(9,27,28,29,30,33))) {
            include("webprofile_includes/amaya_header.php");
        } */
?>
<? if ($prfarr["musicyesno"]=='yes' && $prfarr["musicarea"]=='header') { ?>
    <div class='music' style=' <? 
	if ($prfarr['musicvalign']=='bottom') echo 'margin-top:'.($prfarr["websitehpheight"]-30).'px;';
	 if ($prfarr['musicvalign']=='middle') echo 'margin-top:'.($prfarr["websitehpheight"]/2-15).'px;';
	 ?>'>
	 <?  include('./music.php'); ?>
</div> <? } ?>
</div>
<? if(trim($prfarr["websitehphtml"])!="") { echo "<div id='websitehphtml' style='height:".$prfarr["websitehpheight"]."px;float:left;margin-top:-".$prfarr["websitehpheight"]."px;'>".$prfarr["websitehphtml"]."</div>"; } ?>		


<? 
//$galleryhtml='<div class="gallerysection" id="webgallery">';
@$galleryhtml.=@$prfarr["websitegalleryhtml"].'</div>';
if($prfarr["websitegalleryposition"]=="Above") 
{
	echo '<div class="gallerysection" id="webgallery">' ?>
    <? if ($prfarr["musicyesno"]=='yes' && $prfarr["musicarea"]=='headergallery') { ?>
    <div class='music' style=' <? 
	if ($prfarr['musicvalign']=='bottom') echo 'margin-top:'.($prfarr["websitegalleryheight"]-30).'px;';
	 if ($prfarr['musicvalign']=='middle') echo 'margin-top:'.($prfarr["websitegalleryheight"]/2-15).'px;';
	 ?>'>
	 <?  include('./music.php'); ?>
</div> <? } ?>
	<? echo $galleryhtml; 
} 
?>
<div class="topbutton">
<table cellpadding="0" cellspacing="0" border="0" align="<? echo $prfarr["websitetopalign"]; ?>"><tr>
<?
	foreach($main_menus as $menu) {
		if($menu["menu_position"]=="Top")
		{	
			if($menu['is_homepage']==1 or $menu['is_hidden']==1)
			{
				$hide_string = "style='display:none'";
			} 
			else 
			{
				$hide_string = "";
			}
							
							$have_sub_menu=recursiveArraySearch($sub_menus, $menu['menu_id'], 'parent_id');
							if(is_int($have_sub_menu))
							{
								if(in_array($menu["function"],$rediectlink))
								{
									echo "<td buttonid='{$menu['menu_id']}' function='{$menu['function']}' class=\"";
									echo "topmenu\" id=\"button{$menu['menu_id']}\" >";
									echo "<a href=\"{$menu['content']}\" target=\"_blank\" class=\"topmenu\" style=\"display:block;\">";
									echo $menu['menu_name'];
									echo "</a>";
									
									echo "</td><td class=\"topmenuspacer\"></td>\n";
								}
								else
								{
									 echo "<td buttonid='{$menu['menu_id']}' class=\"action_button topmenu\" ";
									 if (!(empty($menu['function']))) echo "function='{$menu['function']}'";
									 echo " id=\"button{$menu['menu_id']}\" onclick=\"javascript:showSubmenu({$menu['menu_id']});\" {$hide_string}>{$menu['menu_name']}</td><td class=\"topmenuspacer\"></td>\n";
								}
						}
						else
						{
							echo "<td buttonid='{$menu['menu_id']}' function='{$menu['function']}' class=\"";
								
								if(!in_array($menu["function"],$rediectlink))
									echo "action_button ";
								
								echo "topmenu\" id=\"button{$menu['menu_id']}\" {$hide_string}>";
								
								if(in_array($menu["function"],$rediectlink)) echo "<a href=\"{$menu['content']}\" target=\"_blank\" class=\"topmenu\" style=\"display:block;\">";								
								
								echo $menu['menu_name'];
								
								if(in_array($menu["function"],$rediectlink)) echo "</a>";
								echo "</td><td class=\"topmenuspacer\"></td>\n";
					 }		
			}
	}							
?>
</tr></table>							
</div>
<? if($prfarr["websitegalleryposition"]=="Below") 
{
	echo '<div class="gallerysection" id="webgallery">'; ?>
<?   if ($prfarr["musicyesno"]=='yes' && $prfarr["musicarea"]=='headergallery') 
	{ ?>
        <div class='music' style='
<?        if ($prfarr['musicvalign']=='bottom') echo 'margin-top:'.($prfarr["websitegalleryheight"]-30).'px;';
		 if ($prfarr['musicvalign']=='middle') echo 'margin-top:'.($prfarr["websitegalleryheight"]/2-15).'px;';
		 	 ?>'>
 		<? include('./music.php'); ?>
		</div>
<? 	} ?>
<? echo $galleryhtml; } ?>

<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" id="maincontener">
		  <tr>
    		<td class="leftpanel">
            
             	<table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top:<? echo ($prfarr["icon_position"]=="left_top")?($contenttop+40):$contenttop; ?>px;">
  				<tr>
			    <td valign="top">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <?php
						
                        foreach($main_menus as $menu) {
						  if($menu["menu_position"]=="Left" or $menu["menu_position"]=="Top")
						  {	
							if($menu['is_homepage']==1 or $menu["menu_position"]=="Top" or $menu['is_hidden']==1) {
								$hide_string = "style='display:none'";
							} else {
								$hide_string = "";
							}
							
                            /* $have_sub_menu_query="select * from website_menu where `parent_id` = ".$menu['menu_id'].";";
								$have_sub_menu=mysql_query($have_sub_menu_query); 
								if(mysql_num_rows($have_sub_menu)>0)
							*/
							$have_sub_menu=recursiveArraySearch($sub_menus, $menu['menu_id'], 'parent_id');
							
							if(is_int($have_sub_menu)) {
												
								//print_r($menu);
                                //if menu has no function, then implement default functionality: open sub menus
                                echo "<tr {$hide_string}>";
								
								if (in_array($menu["function"],$rediectlink))
								{
									echo "<td buttonid='{$menu['menu_id']}' function='{$menu['function']}' class=\"";
									echo "menu\" id=\"button{$menu['menu_id']}\">";
										echo "<a href=\"{$menu['content']}\" target=\"_blank\" class=\"menu\" style=\"display:block;line-height:".$prfarr['websitenvheight']."px;\">";	
									echo $menu['menu_name'];
									echo "</a></td>";
									
								}
								else
								{
									echo "<td buttonid='{$menu['menu_id']}' class=\"menu";
									if (!(empty($menu['function']))) {echo " action_button\"; function='{$menu['function']}'";} else { echo "\""; }
									echo "id=\"button{$menu['menu_id']}\" onclick=\"javascript:showSubmenu({$menu['menu_id']});\">{$menu['menu_name']}</td>";
								}
								echo "</tr><tr {$hide_string}><td class=\"spacer\"></td></tr>\n";
                                
								$submenucount=0;
                                foreach($sub_menus as $submenu) {
									if($submenu['parent_id']==$menu['menu_id'] && $submenu['menu_position']=="Left")  
									{
                                    	if($submenucount==0) echo "<tr><td>\n<div buttonid='{$submenu['menu_id']}' class='submenu' id='submenu{$menu['menu_id']}'>"; 	
										$submenucount++;
									echo "<div class='";
									
									if(!in_array($submenu['function'],$rediectlink))
									echo "action_button ";
									
									echo "submenu_item' ";
									if ($submenu['is_hidden']==1) echo "style='display:none'";
									echo "function='{$submenu['function']}' buttonid='{$submenu['menu_id']}' id=\"button{$submenu['menu_id']}\">";
									
									// if redirect link then set link
									if(in_array($submenu['function'],$rediectlink))
											echo "<a href=\"{$submenu['content']}\" target=\"_blank\" style=\"display:block;text-decoration:none;\">";
									
									echo $submenu['menu_name'];
									
									if(in_array($submenu['function'],$rediectlink)) echo "</a>";
									
									echo "</div>\n";
									
                                    }
                                }
								 if($submenucount>0) echo "</div>\n</td></tr>\n";
                                
                            } else {
                                
								echo "<tr {$hide_string}><td buttonid='{$menu['menu_id']}' function='{$menu['function']}' class=\"";
								
								if(!in_array($menu["function"],$rediectlink))
									echo "action_button ";
								
								echo "menu\" id=\"button{$menu['menu_id']}\">";
								
								if(in_array($menu["function"],$rediectlink)) echo "<a href=\"{$menu['content']}\" target=\"_blank\" class=\"menu\" style=\"display:block;line-height:".$prfarr['websitenvheight']."px;\">";								
								
								echo $menu['menu_name'];
								
								if(in_array($menu["function"],$rediectlink)) echo "</a>";
								
								echo "</td></tr><tr {$hide_string}><td class=\"spacer\"></td></tr>";
								
                            }
						  } // left menu position condition	
                        }
						                      
?>
<?php
    if($prfarr['hours']!="") {
?>
<tr>
<td class="menu unclickable" id="contactid"><span class="menulink">Hours</span></td>
</tr>
<? } ?>
</table>
</td>
</tr>
</table>
<?  if($prfarr['hours']!="") { ?>
<div id="hoursOfOperation" style='text-align:left;margin:10px;'> 
<? }
?>
<div class="entry"> 
<?php
echo $prfarr['hours'];
?>
</div> 

<?  if($prfarr['hours']!="") { ?>
</div> 
<? }
?>
<?php
    if($prfarr['websitelphtml']!="") {
?>
<div><? echo $prfarr['websitelphtml']; ?></div>
<? } ?>

<div class="social_media">
<? if($prfarr["wlink"]!="") { ?>
    <a href="<? echo $prfarr["wlink"];  ?>" target="_blank"><img src="images/socialmedia_web.png" alt="Web" /></a><? } ?> <? if($prfarr["tlink"]!="") { ?> <a href="<? echo $prfarr["tlink"];  ?>" target="_blank"><img src="images/socialmedia_twitter.png" alt="Twitter" /></a><? } ?> <? if($prfarr["flink"]!="") { ?> <a href="<? echo $prfarr["flink"];  ?>" target="_blank"><img src="images/socialmedia_facebook.png" /></a><? } ?>
    </div>
 </td>
    <td class="contentpanel" id="contentid">
    <?php 
		$cpwidth=(int)1024-$prfarr['websitelpwidth']-$prfarr['websiterpwidth']-20;
		
		$btnfunction=$homepage_button["function"];
		if($btnfunction=="showHTML")
		{
			
			echo $homepage_button["content"];
		}
		else if($btnfunction=="showGallery")
		{

				require("./includes/ImageGallery.php");
				require("./includes/ScrollGallery.php");

				$galleryId = $homepage_button['menu_id'];
				
				$request = array(
    		    'id' => CLIENT_API_ID,
		        'api_secret' => CLIENT_API_KEY,
       			'request' => array(
	            'name' => "getgalleryimages",
				'arguments' => array(
						'galleryid' =>$galleryId,
					)	
            	),
     		);
			$res = curl_post(API_LINK, array("request"=>json_encode($request)));
			$galleryarr = json_decode($res, true);	
			
			$files = array();
			foreach($galleryarr as $row)
			{
				  $file['name'] = $row['image_path'];
       			  $file['galleryid'] = $galleryId;
        	 	  $file['imageid'] = $row['image_id'];
        		  $files[] = $file;	
			}
			
			if (sizeof($files) <= 1) {
     	   $settings = array("gallery_id" => $galleryId, "container_v_padding" => 0, "container_h_padding" => 0, "v_spacing" => 0, "h_spacing" => 0, "extra_space_h" => 0, "width" => $cpwidth, "height" => 580, "max_image_height" => 580, "caller" => $_SERVER['PHP_SELF'], "col_limit" => 1);
   		 } else {
       	 $settings = array("gallery_id" => $galleryId, "v_spacing" => 5, "h_spacing" => 5, "width" => $cpwidth, "height" => 580, "max_image_height" => 260, "caller" => $_SERVER['PHP_SELF'], "col_limit" => 3);
   	 }
    	if (empty($_GET['page'])) {
        	$page = 1;
    	} else {
        	$page = $_GET['page'];
    	}
		
    	$imageGallery = new ImageGallery($files, $settings);
    	$code = $imageGallery->displayPage($page);
    	echo $code;
			
	}
		else if($btnfunction=="lightboxFullGallery")
		{
				?>
                <script language="javascript">
					lightboxgallery("<?=$homepage_button['menu_id']?>","<?=$homepage_button['lightboxwidth']?>","<?=$homepage_button['lightboxheight']?>","<?=$homepage_button['content']?>");
				</script>		
         <?       
		}
		else if($btnfunction=="slideshowGallery")
		{
			$pausetime=$homepage_button['pausetime'];
			?>
            <center>
			<div id="slideshow" pause="<? echo $pausetime; ?>000">
            <?
				$galleryId = $homepage_button['menu_id'];
				
				$request = array(
    		    'id' => CLIENT_API_ID,
		        'api_secret' => CLIENT_API_KEY,
       			'request' => array(
	            'name' => "getgalleryimages",
				'arguments' => array(
						'galleryid' =>$galleryId,
					)	
            	),
     		);
			$res = curl_post(API_LINK, array("request"=>json_encode($request)));
			$galleryarr = json_decode($res, true);	
			$imgcount=0;
			foreach($galleryarr as $row)
			{
					$isize=@getimagesize($row['image_path']);
					$size="";
					$iwidth=$row['image_width'];
					$iheight=$row['image_height'];
					if($iwidth>$cpwidth or $iheight>570)
					{
						$wratio=$iwidth/$cpwidth;
						$hratio=$iheight/570;
						if($wratio>=$hratio)
						{
							$iwidth=$cpwidth;
							$iheight=($iheight/$wratio);
						}
						else
						{
							$iwidth=($iwidth/$hratio);
							$iheight=570;		
						}
				}
				$size=" width=\"".$iwidth."\" height=\"".$iheight."\" hspace=\"".(($cpwidth-$iwidth)/2)."\" vspace=\"".((570-$iheight)/2)."\" "; 	
		?>
		<img src="<? echo $row['image_path']; ?>" <? if($imgcount==0) echo " class=\"active\" "; ?> <? echo $size; ?> />
   		<? 
	 	$imgcount++;
				
			}	
			
			?>
            </div></center>  	
            <script language="javascript">
			setInterval( "slideSwitch()", "<? echo $pausetime; ?>000");
			</script>  
            <?
		}
		else if($btnfunction=="ShowMap")
		{
			echo "<iframe name=\"contact_map\" id=\"contact_map\" width=\"100%\" height=\"568\" frameborder=\"0\" scrolling=\"no\" marginheight=\"0\" marginwidth=\"0\" src=\"http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=".$contact_address."&amp;z=14&amp;output=embed\"></iframe>";
		}
		else  if($btnfunction=="showSpecial")
		{
			$res = curl_post(WEB_DATA_URL."dailyspecial.php",array("menu_id"=>$homepage_button['pausetime']));
			echo $res;	
		}
		else  if($btnfunction=="ShowEnquiry")
		{
			$res = curl_post(WEB_DATA_URL."profile_ops.php",array("datatype"=>"inquiryform"));
			echo $res;	
		}
		else  if($btnfunction=="showPrivatePartyForm")
		{
			$res = curl_post(WEB_DATA_URL."profile_ops.php",array("datatype"=>"showPrivatePartyForm"));
			echo $res;	
		}
		else  if($btnfunction=="showSubscribeNewsletterForm")
		{
			$res = curl_post(WEB_DATA_URL."profile_ops.php",array("datatype"=>"showSubscribeNewsletterForm"));
			echo $res;	
		}
		else  if($btnfunction=="RedirectLink")
		{
				//header("Location:".$homepage_button["content"]);
		?>
			<script language="javascript">
				window.location = '<? echo $homepage_button["content"]; ?>';
			</script>
           <?     
		}
	//echo $homepage_button['content']; 
		//print_r($prfarr);
		//print_r($prfarr['website_buttons']);
    ?>
    </td>
    <td class="rightpanel">

    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top:
    <?php
        // echo top-margin for table on the right panel
        /* if($web_profile_id==37) {
            //firkinspub
            echo "140";
        } else {
		*/
            echo $contenttop; 
        //}
    ?>px;">
      <tr>
    <td height="225" valign="top">
    
    <table width="100%" border="0" cellspacing="0" cellpadding="0"> 
   
    	<? if($prfarr["showgeneralinfo"]=="Yes") { // show general info tab
				?>
              
                	<? if($prfarr["weblocation"]!="" || $prfarr["webcity"]!="" || $prfarr["webstate"]!="" || $prfarr["webcountry"]!="" || $prfarr["webpostalcode"]!="" || $prfarr["webphone"]!="" || $prfarr["webemail"]!="") { ?>
        			<tr>
            		<td class="menu" id="contactid"><span class="menulink"><? echo setcase("general info",$prfarr["rightpanelfontcase"]); ?></span></td>
            		</tr>
            	<? }  ?>
            	<? if($prfarr["weblocation"]!="") { ?>
                <tr>
                    <td class="address"><? echo $prfarr["weblocation"]; ?></td>
                 </tr>
                <? }  ?>
                <? if($prfarr["webcity"]!="" || $prfarr["webstate"]!="" || $prfarr["webcountry"]!="" || $prfarr["webpostalcode"]!="") { ?>
                        <tr>
                            <td class="address"><? 
								if($prfarr["webcity"]!="") echo $prfarr["webcity"].", ";
								if($prfarr["webstate"]!="") echo $prfarr["webstate"].", ";
								if($prfarr["webcountry"]!="") echo $prfarr["webcountry"].", ";
								echo $prfarr["webpostalcode"];
							 ?></td>
                 </tr>
                 <? } ?>
                   <? if($prfarr["webphone"]!="") { ?>
                        <tr>
                             <td class="address"><img src="images/phone_icon.png" align="absmiddle" /> : <? echo $prfarr["webphone"]; ?></td>
                         </tr>
                     <? } ?>
                     <? if($prfarr["webemail"]!="") { ?>
                        <tr>
                         <td class="address"><img src="images/email_icon.png" align="absmiddle" /> : <a href="mailto:<? echo $prfarr["webemail"]; ?>">Contact</a></td>
                        
                    <? } ?>
                     </tr>
                         
                <?
			}
    	?>
	<? if($prfarr["rightpaneldynamic"]!="Yes") { // Fix right panel 
        //amaya
        if(in_array($prfarr["z_webid_pk"],array(9))) {
            include("webprofile_includes/amaya.php");
        } 
		else {
    ?>
    			
             <? if($prfarr["weblocation"]!="" || $prfarr["webcity"]!="" || $prfarr["webstate"]!="" || $prfarr["webcountry"]!="" || $prfarr["webpostalcode"]!="") { ?>
                                                <tr>
                                                    <td class="menu" id="locationid" onclick="javascript:showmap(this.id);" onmouseover="javascript:menuover(this);" onmouseout="javascript:menuout(this);"><? echo setcase("location map",$prfarr["rightpanelfontcase"]); ?></td>
                                                    </tr>
                                                    <tr><td class="spacer"></td></tr>
                                                    <? }  ?>
                                                    <tr>
													<?php
														//bodypump
														if($prfarr['z_webid_pk']==26) {
															?>
																<td buttonid='190' function='showBookYourVisitTodayForm' class="action_button menu" id="button190">Book A Visit</td>
															<?
														} else if(in_array($prfarr['z_webid_pk'],array(13,27,30))) {//jacknjills
															
														} else if($prfarr['z_webid_pk'] ==23) {//bottlerock//profile_user = 'bottorockla'
															?>
															<td class="menu" id="enquiryid" onclick='javascript:showBottleRockOpenTableReservation()' onmouseover="javascript:menuover(this);" onmouseout="javascript:menuout(this);"><? echo setcase("reservation",$prfarr["rightpanelfontcase"]); ?>
															</td>
															<?
														} 
														else if($prfarr['z_webid_pk'] ==28)
														{
														?>
                                                        <td class="menu" id="enquiryid" onclick='javascript:gotoUrl("http://www.opentable.com/amaya-the-indian-room-reservations-toronto?restref=14218")' onmouseover="javascript:menuover(this);" onmouseout="javascript:menuout(this);"><? echo setcase("reservation",$prfarr["rightpanelfontcase"]); ?>
															</td>
                                                        <?
														}
														else {
															?>
															<td class="menu" id="enquiryid" onclick="javascript:showinquiry(this.id);" onmouseover="javascript:menuover(this);" onmouseout="javascript:menuout(this);"><? echo setcase("reservation",$prfarr["rightpanelfontcase"]); ?></td>
															<?
														}
													?>
                                                    </tr>
                                                    <? } ?>
                                                  
                                                   <? } else {  // dynamic right panel 
                                                    	?>
                                             
                                                        <?
														foreach($main_menus as $menu) {
						 								 if($menu["menu_position"]=="Right" or $menu["menu_position"]=="Top")
														  {	
																if($menu['is_homepage']==1 or $menu["menu_position"]=="Top" or $menu['is_hidden']==1) {
																$hide_string = "style='display:none'";
																} else {
																$hide_string = "";
																}
															
															/* $have_sub_menu_query="select * from website_menu where `parent_id` = ".$menu['menu_id'].";";
															$have_sub_menu=mysql_query($have_sub_menu_query);
															if(mysql_num_rows($have_sub_menu)>0)
															{ */
															$have_sub_menu=recursiveArraySearch($sub_menus, $menu['menu_id'], 'parent_id');
															if(is_int($have_sub_menu)) 
															{
								                                //if menu has no function, then implement default functionality: open sub menus
                                								echo "<tr {$hide_string}>";
																if (in_array($menu["function"],$rediectlink))
																{
																	echo "<td buttonid='{$menu['menu_id']}' function='{$menu['function']}' class=\"";
																	echo "menu\" id=\"button{$menu['menu_id']}\">";
																	echo "<a href=\"{$menu['content']}\" target=\"_blank\" class=\"menu\" style=\"display:block;line-height:30px;\">";
																	echo $menu['menu_name'];
																	echo "</a></td>";
																}
																else 
																{
																	echo "<td class=\"action_button menu\" ";
																	if (!(empty($menu['function']))) echo "function='{$menu['function']}'";
																	echo " id=\"button{$menu['menu_id']}\" onclick=\"javascript:showSubmenu({$menu['menu_id']});\">{$menu['menu_name']}</td>";
																}
																echo "</tr><tr {$hide_string}><td class=\"spacer\"></td></tr>\n";
							                                
                            						    		$submenucount=0;
																foreach($sub_menus as $submenu) {
                                    							if($submenu['parent_id']==$menu['menu_id'] && $submenu['menu_position']=="Right")  {
																if($submenucount==0) echo "<tr><td>\n<div buttonid='{$submenu['menu_id']}' class='submenu' id='submenu{$menu['menu_id']}'>";
																$submenucount++;
																echo "<div class='";
																if(!in_array($submenu["function"],$rediectlink))
																echo "action_button ";
																echo "submenu_item' ";
																if ($submenu['is_hidden']==1) echo "style='display:none'";
																echo "function='{$submenu['function']}' buttonid='{$submenu['menu_id']}' id=\"button{$submenu['menu_id']}\">";
															
																if(in_array($submenu["function"],$rediectlink)) echo "<a href=\"{$submenu['content']}\" target=\"_blank\" style=\"display:block;text-decoration:none;\">";
															
																echo $submenu['menu_name'];
															
																if(in_array($submenu["function"],$rediectlink)) echo "</a>";
																echo "</div>\n";
                                    							}
                    				           			 }
														 if($submenucount>0) echo "</div>\n</td></tr>\n";
					                            } else {
                                				echo "<tr {$hide_string}><td buttonid='{$menu['menu_id']}' function='{$menu['function']}' class=\"";
												
												if(!in_array($menu["function"],$rediectlink)) echo "action_button ";
												
												echo "menu\" id=\"button{$menu['menu_id']}\">";
												
												if(in_array($menu["function"],$rediectlink)) echo "<a href=\"{$menu['content']}\" target=\"_blank\" class=\"menu\" style=\"display:block;line-height:30px;\">";
												echo $menu['menu_name'];
												if(in_array($menu["function"],$rediectlink)) echo "</a>";
												echo "</td></tr><tr><td class=\"spacer\" {$hide_string}></td></tr>";
			                        		    }
										  } // right menu position condition	
		                    		    }
													
													
													 } ?>
                                                     </table>
                                                    	<?php
													    if($prfarr['websiterphtml']!="") {
														?>
																<div><? echo $prfarr['websiterphtml']; ?></div>
														<? } ?>
                                                        
                                                    </td>
                                                    </tr>
                                                    

                                                    <tr>
                                                    <td align="center">
                                                    <? 
                                                    if($prfarr["z_apiid_small"]!=0) 
													{
														
														$request = array(
														        'id' => CLIENT_API_ID,
														        'api_secret' => CLIENT_API_KEY,
														        'request' => array(
															     'name' => "getmenuboxapi",
																'arguments' => array(
																				'apiid' => $prfarr["z_apiid_small"]
																		)	
												        		    ),
     													);
														$res = curl_post(API_LINK, array("request"=>json_encode($request)));
														$apares = json_decode($res, true);
    	
														/* $apsres=mysql_query("select * from menubox_api where z_apiid_pk=".$prfarr["z_apiid_small"]); 
    													if(mysql_num_rows($apsres)>0)
    													{	
        													$apares=mysql_fetch_array($apsres); */
														if(is_array($apares))	 {
       														 ?>
            												<? if($prfarr["z_apiid_large"]!=0) { ?> 	
                											<div style="width:207px;height:<? echo $apares["api_height"] ?>px; margin-left: auto;margin-right: auto;text-align:center;
position:absolute;" align="center">
             												<a href="javascript:openpopup();">
             															<img src="images/spacer.gif" width="<? echo $apares["api_width"] ?>" height="<? echo $apares["api_height"] ?>" border="0" /></a>
             												</div>
           													 <? } ?>			 
             												<? echo $apares["api_html"]; ?>
             											<? 
    														}	// if no record
															 //   mysql_free_result($apsres);	
													}  // if small id 0
												?>
<? if($prfarr["z_apiid_large"]!=0) { 
  											  $request = array(
														        'id' => CLIENT_API_ID,
														        'api_secret' => CLIENT_API_KEY,
														        'request' => array(
															     'name' => "getmenuboxapi",
																'arguments' => array(
																				'apiid' => $prfarr["z_apiid_large"]
																		)	
												        		    ),
     													);
														$res = curl_post(API_LINK, array("request"=>json_encode($request)));
														$apares = json_decode($res, true);
	/* $apsres=mysql_query("select * from menubox_api where z_apiid_pk=".$prfarr["z_apiid_large"]); 
    if(mysql_num_rows($apsres)>0)
    {	
	 $apares=mysql_fetch_array($apsres);	*/
		// if no record
    //mysql_free_result($apsres);	
} ?> 
</td> 
</tr>
</table></td>
</tr>
</table>
</td>
</tr>
</table>
<?
  if(is_array(@$apares))	 {					
        ?>  
            <div id="blackoverlay" class="overlay">
            </div>
            <div id="largeapi" class="popupapi">
            <div style="position:absolute;margin-top:-17px;margin-left:<? echo ($apares["api_width"]-23) ?>px;background-color:transparent;">
            <a href="javascript:closepopup();"><img src="./images/api_popupclose.png" alt="Close" border="0" /></a>
            </div>
            <? echo $apares["api_html"]; ?>	 	
            </div> 
            <? 
    }	
/*echo "<textarea rows='30' cols='80'>";
htmlspecialchars(print_r($main_menus));
echo "</textarea>";*/
?>

<div class="footer">
<div style="float:left;width:920px;">
<? if ($prfarr["musicyesno"]=='yes'  && $prfarr["musicarea"]=='footer') 
{	?>
      <div class='music' style="
<?	  if ($prfarr['musicvalign']=='bottom') echo 'margin-top:'.($prfarr["websitefpheight"]-30).'px;';
 if ($prfarr['musicvalign']=='middle') echo 'margin-top:'.($prfarr["websitefpheight"]/2-15).'px;';
	  ?>">
     <? // include('./music.php'); ?>
</div>
<?
} 
?>
<? echo $prfarr["footer_html"]; ?>
</div>
<div style="" class="dmb_logo">
<a href="http://www.unoapp.com" target="_blank"><img src="images/<? echo $prfarr["dmb_logo"]?>" border="0" height="40" /></a></div>
</div>
</div>
<div id='msg'></div><input type="hidden" name="cwidth" id="cwidth" value="<?=$cpwidth?>" />
 </body>
</html>
