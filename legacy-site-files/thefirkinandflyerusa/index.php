<?php
	//set_time_limit(60);
	include("./includes/client.php");
	
	$request = array(
        'id' => CLIENT_API_ID,
        'api_secret' => CLIENT_API_KEY,
        'request' => array(
            'name' => "getWebProfileInfo",
			'arguments' => array(
					'profile' => PROFILE
				)	
            ),
     );
	$res = curl_post(API_LINK, array("request"=>json_encode($request)));
	
	$res=utf8_encode($res);
		
	$prfarr = json_decode($res,  $assoc = true);
	/*print_r($prfarr);
	exit();*/
			
	@$pagename=$_GET["pg"];
	if(strpos($pagename,"/")!==false)
	{
		if(strpos($pagename,"/")==strlen($pagename)-1) $pagename=substr($pagename,0,strlen($pagename)-1);  
	}	
	
	$imagepath="webdata/".strtolower(PROFILEID)."/";	
	$contact_address=$prfarr["weblocation"].", ".$prfarr["webcity"].", ".$prfarr["webstate"].", ".$prfarr["webcountry"].", ".$prfarr["webpostalcode"];
	$contenttop=$prfarr["websitenvtop"];
	
	switch($prfarr["templateid_fk"])
	{
		case 2: 
				$csslink="webcss/variable_fontstyle.css";
				break;
		default: 
				$csslink="webcss/fixed_fontstyle.css";	
				break;
	}
	
	$main_menus = array();
	$sub_menus = array();
	$homepage_button = array();
	$btns=array();
	$redrlink=array();
	$spchar=array(" ","/","_");
	foreach($prfarr['website_buttons'] as $menu) {
		$btns[$menu['menu_id']]=str_replace($spchar,"_",$menu['menu_name']);
		//echo $menu['menu_name']." ".urlencode($menu['menu_name'])."<br>";
		if($menu["function"]=="RedirectLink") $redrlink[$menu['menu_id']]=$menu["content"];
		
		if($menu['parent_id']==0) {
			$main_menus[] = $menu;
			if($menu['is_homepage'] == 1 && count($homepage_button)==0) {
				$homepage_button = $menu;
			}
		} else {
			$sub_menus[] = $menu;
		}
			//echo strtoupper(str_replace($spchar,"",$menu['menu_name']))." ".strtoupper(str_replace($spchar,"",$pagename))."<br>";
			//if(strtoupper($menu['menu_name'])==stripslashes(strtoupper(str_replace($spchar," ",$pagename)))) 
			if(strtoupper(str_replace($spchar," ",$menu['menu_name']))==stripslashes(strtoupper(str_replace($spchar," ",$pagename)))) 
			{
				$homepage_button = $menu;
			}	
	} // for loop end here
	$main_menus=array_sort($main_menus, 'menu_order', SORT_ASC);
	$sub_menus=array_sort($sub_menus, 'menu_order', SORT_ASC);
	
	//print_r($sub_menus);
	//exit();
	
	$rediectlink=array("RedirectLink","showOpenTableReservation");
	
	if($prfarr["hasshortlink"]!=1)
	{
		$pglink="http://".$_SERVER['HTTP_HOST']."/".HOST_FOLDER.substr($_SERVER['SCRIPT_NAME'],strrpos($_SERVER['SCRIPT_NAME'],"/")+1)."?pg=";
     }
	 else
	 {
	 	$pglink="http://".$_SERVER['HTTP_HOST']."/".HOST_FOLDER;
	 }  
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>



<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="Content-Language" content="EN">
<title><?php ?><?php echo ($prfarr["webtitle"]!="")?$prfarr["webtitle"]:PROFILE; ?><?php ?></title>

<meta name="title" content="<?php echo ($prfarr["webtitle"]!="")?$prfarr["webtitle"]:PROFILE; ?>" />
<meta name="keywords" content="<?php echo ($prfarr["webkeyword"]!="")?$prfarr["webkeyword"]:PROFILE; ?>" />
<meta name="description" content="<?php echo ($prfarr["webdescription"]!="")?$prfarr["webdescription"]:PROFILE; ?>" />
<META name="expires" content="never">
<META name="charset" content="UTF-8">
<META NAME="AUTHOR" CONTENT="Digital Menubox">
<META NAME="COPYRIGHT" CONTENT="Copyright@2010 <?php echo PROFILE; ?>">
<META NAME="DISTRIBUTION" CONTENT="Global">
<META NAME="RATING" CONTENT="General">
<META NAME="ROBOTS" CONTENT="Index, Follow">
<?
 if(trim($prfarr["analyticaccount"])!="") { ?>
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', '<? echo $prfarr["analyticaccount"] ?>']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
  
  
</script>
<? }
 ?>
<base href="<?php echo WEB_DATA_URL; ?>">
<? 
     
	
	echo getExtCssString($prfarr["websitefonttype"]);
	echo getExtCssString($prfarr["websitetopnvfonttype"]);
	echo getExtCssString($prfarr["websitesubnvfonttype"]);
 ?>

<link rel="stylesheet" href="<?php echo $csslink; ?>" />
<script language="javascript" src="includes/webjavascript.js"></script>
<script language="javascript" src="https://code.jquery.com/jquery-1.11.2.js"></script>
<script language="javascript" src="./js/galleria.js"></script>
<script language="javascript" src="./js/galleria.classic.js"></script>
<script language="javascript" src="js/thickbox.js"></script>
<script type="text/javascript" src="js/jquery.form.js"></script>
<!-- Calling Fancybox -->
<script type="text/javascript" src="http://www.digitalmarketingbox.com/webtool/js/fancybox2/jquery.fancybox.js"></script>
<link href="http://www.digitalmarketingbox.com/webtool/js/fancybox2/jquery.fancybox.css" media="screen" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="http://www.digitalmarketingbox.com/webtool/js/flexslider/jquery.flexslider.js"></script>
<link rel="stylesheet" type="text/css" href="http://www.digitalmarketingbox.com/webtool/js/flexslider/flexslider.css">

<style type="text/css">
body {
	<? if($prfarr["webpgbgtype"]=="Color" && $prfarr["webpgbgvalue"]!="") { ?>
	background-color: <? echo $prfarr["webpgbgvalue"]; ?>;
	<? } ?>
	<? if($prfarr["webpgbgtype"]=="Image" && $prfarr["webpgbgvalue"]!="" && fileexists($imagepath.$prfarr["webpgbgvalue"])) { ?>
	background-image:url(<? echo $imagepath.$prfarr["webpgbgvalue"]; ?>);
	<? } ?>
	<? if($prfarr["websitefonttype"]!="") { ?> 
	font-family: <? echo $prfarr["websitefonttype"]; ?>;
	<? } ?>
	<? if($prfarr["websitefontsize"]!="") { ?>
	font-size: <? echo $prfarr["websitefontsize"]; ?>px;
	<? } ?>
	<? if($prfarr["websitefontcolor"]!="") { ?>	
	color: <? echo $prfarr["websitefontcolor"]; ?>;
	<? } ?>
	
	<? if(strpos($prfarr["websitefontstyle"],"Bold")!==false) { ?>
	font-weight:bold;
	<? } else { ?>
	font-weight:normal;
	<? } ?>
	
	 <? if(strpos($prfarr["websitefontstyle"],"Italic")!==false) { ?>
	font-style:italic;
	<? } else { ?>
	font-style:normal;
	<? } ?>
	
	<? if(strpos($prfarr["websitefontstyle"],"Underline")!==false) { ?>
	text-decoration:underline;
	<? } else { ?>
	text-decoration:none;
	<? } ?>
}
a:link, a:visited, a:active {
	<? if($prfarr["websitefontcolor"]!="") { ?>
	color: <? echo $prfarr["websitefontcolor"]; ?>;
	<? } ?>
	text-decoration: none;
}
a:hover {
	<? if($prfarr["websitefontcolor"]!="") { ?>
	color: <? echo $prfarr["websitefontcolor"]; ?>;
	<? } ?>
	text-decoration: underline;	
}
.inputtext, .button
{
	<? if($prfarr["websitefonttype"]!="") { ?> 
	font-family: <? echo $prfarr["websitefonttype"]; ?>;
	<? } ?>
	<? if($prfarr["websitefontsize"]!="") { ?>
	font-size: <? echo $prfarr["websitefontsize"]; ?>px;
	<? } ?>
	<? if($prfarr["websitefontcolor"]!="") { ?>	
	color: #000000; 
	<? } ?>
}
.website
{
	<? if($prfarr["websitebgtype"]=="Color" && $prfarr["websitebgvalue"]!="") { ?>
	background-color: <? echo $prfarr["websitebgvalue"]; ?>;
	<? } ?>
	<? if($prfarr["websitebgtype"]=="Image" && $prfarr["websitebgvalue"]!="" && fileexists($imagepath.$prfarr["websitebgvalue"])) { ?>
	background-image:url(<? echo $imagepath.$prfarr["websitebgvalue"]; ?>);
	<? } ?>
}
.web_header
{
	<? if($prfarr["websitehpbgtype"]=="Color" && $prfarr["websitehpbgvalue"]!="") { ?>
	background-color: <? echo $prfarr["websitehpbgvalue"]; ?>;
	<? } ?>
	<? if($prfarr["websitehpbgtype"]=="Image" && $prfarr["websitehpbgvalue"]!="" && fileexists($imagepath.$prfarr["webpgbgvalue"])) { ?>
	background-image:url(<? echo $imagepath.$prfarr["websitehpbgvalue"]; ?>);
	background-repeat:no-repeat;
	<? } ?>
	<? if($prfarr["websitehpheight"]!=0 || 1==1) {?>
	height: <? echo $prfarr["websitehpheight"]; ?>px;
	<? } ?>
}
.footer
{
	<? if($prfarr["websitefpbgtype"]=="Color" && $prfarr["websitefpbgvalue"]!="") { ?>
	background-color: <? echo $prfarr["websitefpbgvalue"]; ?>;
	<? } ?>
	<? if($prfarr["websitefpbgtype"]=="Image" && $prfarr["websitefpbgvalue"]!="" && fileexists($imagepath.$prfarr["websitefpbgvalue"])) { ?>
	background-image:url(<? echo $imagepath.$prfarr["websitefpbgvalue"]; ?>);
	<? } ?>
	<? if($prfarr["websitefpbgtype"]=="Color" && $prfarr["websitefpbgvalue"]!="") { ?>
	background-color: <? echo $prfarr["websitefpbgvalue"]; ?>;
	<? } ?>
	<? if($prfarr["websitefpbgtype"]=="Image" && $prfarr["websitefpbgvalue"]!="" && fileexists($imagepath.$prfarr["websitefpbgvalue"])) { ?>
	background-image:url(<? echo $imagepath.$prfarr["websitefpbgvalue"]; ?>);
	<? } ?>
	<? if($prfarr["websitefpfontsize"]!="") { ?> 
	font-size: <? echo $prfarr["websitefpfontsize"]; ?>px;
	<? } ?>
	<? if($prfarr["websitefpfontcolor"]!="") { ?> 
	color: <? echo $prfarr["websitefpfontcolor"]; ?>;
	<? } ?>
	
	<? if(strpos($prfarr["websitefpfontstyle"],"Bold")!==false) { ?>
	font-weight:bold;
	<? } else { ?>
	font-weight:normal;
	<? } ?>
	
	 <? if(strpos($prfarr["websitefpfontstyle"],"Italic")!==false) { ?>
	font-style:italic;
	<? } 
	else { ?>
	font-style:normal;
	<? } ?>
	
	<? if(strpos($prfarr["websitefpfontstyle"],"Underline")!==false) { ?>
	text-decoration:underline;
	<? } else { ?>
	text-decoration:none;
	<? } ?>
}
.footer a:link, .footer a:visited, .footer a:active {
	<? if($prfarr["websitefpfontsize"]!="") { ?> 
	font-size: <? echo $prfarr["websitefpfontsize"]; ?>px;
	<? } ?>
	<? if($prfarr["websitefpfontcolor"]!="") { ?> 
	color: <? echo $prfarr["websitefpfontcolor"]; ?>;
	<? } ?>
	
	<? if(strpos($prfarr["websitefpfontstyle"],"Bold")!==false) { ?>
	font-weight:bold;
	<? } else { ?>
	font-weight:normal;
	<? } ?>
	
	 <? if(strpos($prfarr["websitefpfontstyle"],"Italic")!==false) { ?>
	font-style:italic;
	<? }else { ?>
	font-style:normal;
	<? } ?>
	
	<? if(strpos($prfarr["websitefpfontstyle"],"Underline")!==false) { ?>
	text-decoration:underline;
	<? } else { ?>
	text-decoration:none;
	<? } ?>
}
.footer a:hover {
	<? if($prfarr["websitefpfontsize"]!="") { ?> 
	font-size: <? echo $prfarr["websitefpfontsize"]; ?>px;
	<? } ?>
	<? if($prfarr["websitefprfontcolor"]!="") { ?> 
	color: <? echo $prfarr["websitefprfontcolor"]; ?>;
	<? } ?>
	
	<? if(strpos($prfarr["websitefprfontstyle"],"Bold")!==false) { ?>
	font-weight:bold;
	<? } else { ?>
	font-weight:normal;
	<? } ?>
	
	 <? if(strpos($prfarr["websitefprfontstyle"],"Italic")!==false) { ?>
	font-style:italic;
	<? }else { ?>
	font-style:normal;
	<? } ?>
	
	<? if(strpos($prfarr["websitefprfontstyle"],"Underline")!==false) { ?>
	text-decoration:underline;
	<? } else { ?>
	text-decoration:none;
	<? } ?>
}
.footer
{
	position:relative;	
}
.leftpanel
{
	
	<? if($prfarr["websitelpbgtype"]=="Color" && $prfarr["websitelpbgvalue"]!="") { ?>
	background-color: <? echo $prfarr["websitelpbgvalue"]; ?>;
	<? } ?>
	<? if($prfarr["websitelpbgtype"]=="Image" && $prfarr["websitelpbgvalue"]!="" && fileexists($imagepath.$prfarr["websitelpbgvalue"])) { ?>
	background-image:url(<? echo $imagepath.$prfarr["websitelpbgvalue"]; ?>);
	<? } ?>
	width:<? echo $prfarr["websitelpwidth"]; ?>px;
	<? if($prfarr["websitelpwidth"]==0) { ?>
		display:none;
	<? } ?>
}
.rightpanel
{
	<? if($prfarr["websiterpbgtype"]=="Color" && $prfarr["websiterpbgvalue"]!="") { ?>
	background-color: <? echo $prfarr["websiterpbgvalue"]; ?>;
	<? } ?>
	<? if($prfarr["websiterpbgtype"]=="Image" && $prfarr["websiterpbgvalue"]!="" && fileexists($imagepath.$prfarr["websiterpbgvalue"])) { ?>
	background-image:url(<? echo $imagepath.$prfarr["websiterpbgvalue"]; ?>);
	<? } ?>
	width:<? echo $prfarr["websiterpwidth"]; ?>px;
	<? if($prfarr["websiterpwidth"]==0) { ?>
		display:none;
	<? } ?>
}

.social_media
{
	margin-left:10px;
	top:<? echo ($prfarr["icon_position"]=="left_top")?$contenttop:675; ?>px; 
	position:absolute; 	
}
.social_media img
{
	padding-right:5px;
	border:0px;
}
.overlay
{
	display: none;
	position: absolute;
	top: 0%;
	left: 0%;
	width: 100%;
	height: 100%;
	background-color: black;
	z-index:0.5;
	-moz-opacity: 0.5;
	opacity:.50;
	filter: alpha(opacity=50);
}
.popupapi
{
	display: none;
	position: absolute;
	top:5%;
	left:30%;
	z-index:200;
}
.menu, .menu:active, .menu:link, .menu:visited
{
	<? if($prfarr["websitenvbgvalue"]!="" && fileexists($imagepath.$prfarr["websitenvbgvalue"])) { ?>
	background-image:url(<? echo $imagepath.$prfarr["websitenvbgvalue"]; ?>);
	<? } ?>
    <? if($prfarr["websitenvfontalign"]!="") { ?> 
	text-align:<? echo $prfarr["websitenvfontalign"]; ?>;
	<? } ?>

	<? if($prfarr["websitenvfonttype"]!="") { ?> 
	font-family: <? echo $prfarr["websitenvfonttype"]; ?>;
	<? } ?>
	<? if($prfarr["websitenvfontsize"]!="") { ?> 
	font-size: <? echo $prfarr["websitenvfontsize"]; ?>px;
	<? } ?>
	<? if($prfarr["websitenvfontcolor"]!="") { ?> 
	color: <? echo $prfarr["websitenvfontcolor"]; ?>;
	<? } ?>
	
	<? if(strpos($prfarr["websitenvfontstyle"],"Bold")!==false) { ?>
	font-weight:bold;
	<? } else { ?>
	font-weight:normal;
	<? } ?>
	
	 <? if(strpos($prfarr["websitenvfontstyle"],"Italic")!==false) { ?>
	font-style:italic;
	<? } else { ?>
	font-style:normal;
	<? } ?>
	
	<? if(strpos($prfarr["websitenvfontstyle"],"Underline")!==false) { ?>
	text-decoration:underline;
	<? } else { ?>
	text-decoration:none;
	<? } ?>
	height:<? echo $prfarr["websitenvheight"]; ?>px;
    <?php if($prfarr["websitenvfontalign"]=="left") { ?>
    text-indent:20px;
    <? } ?>
    cursor: pointer;
}
.menuselected
{
    <? if($prfarr["websitenvfontalign"]!="") { ?> 
	text-align:<? echo $prfarr["websitenvfontalign"]; ?>;
	<? } ?>
	<? if($prfarr["websitenvrbgvalue"]!="" && fileexists($imagepath.$prfarr["websitenvrbgvalue"])) { ?>
	background-image:url(<? echo $imagepath.$prfarr["websitenvrbgvalue"]; ?>);
	<? } ?>
	<? if($prfarr["websitenvfonttype"]!="") { ?> 
	font-family: <? echo $prfarr["websitenvfonttype"]; ?>;
	<? } ?>
	<? if($prfarr["websitenvfontsize"]!="") { ?> 
	font-size: <? echo $prfarr["websitenvfontsize"]; ?>px;
	<? } ?>
	<? if($prfarr["websitenvrfontcolor"]!="") { ?> 
	color: <? echo $prfarr["websitenvrfontcolor"]; ?>;
	<? } ?>
	
	<? if(strpos($prfarr["websitenvrfontstyle"],"Bold")!==false) { ?>
	font-weight:bold;
	<? } else { ?>
	font-weight:normal;
	<? } ?>
	
	 <? if(strpos($prfarr["websitenvrfontstyle"],"Italic")!==false) { ?>
	font-style:italic;
	<? } else { ?>
	font-style:normal;
	<? } ?>
	
	<? if(strpos($prfarr["websitenvrfontstyle"],"Underline")!==false) { ?>
	text-decoration:underline;
	<? } else { ?>
	text-decoration:none;
	<? } ?>
	height:<? echo $prfarr["websitenvheight"]; ?>px;
    <?php if($prfarr["websitenvfontalign"]=="left") { ?>
    text-indent:20px;
    <? } ?>
}
.topbutton
{
	height:<? echo $prfarr["websitetopheight"]; ?>px;
	<? if($prfarr["websitetopbgvalue"]!="" && fileexists($imagepath.$prfarr["websitetopbgvalue"])) { ?>
	background-image:url(<? echo $imagepath.$prfarr["websitetopbgvalue"]; ?>);
	<? } ?>
	overflow:hidden;
}
.topmenu, .topmenu:active, .topmenu:link, .topmenu:visited
{
	width:<? echo $prfarr["websitetopnvwidth"]; ?>px;
	height:<? echo $prfarr["websitetopheight"]; ?>px;
	<? if($prfarr["websitetopnvbgvalue"]!="" && fileexists($imagepath.$prfarr["websitetopnvbgvalue"])) { ?>
	background-image:url(<? echo $imagepath.$prfarr["websitetopnvbgvalue"]; ?>);
	<? } ?>
    <? if($prfarr["websitetopnvfontalign"]!="") { ?> 
	text-align:<? echo $prfarr["websitetopnvfontalign"]; ?>;
	<? } ?>
	<? if($prfarr["websitetopnvfonttype"]!="") { ?> 
	font-family: <? echo $prfarr["websitetopnvfonttype"]; ?>;
	<? } ?>
	<? if($prfarr["websitetopnvfontsize"]!="") { ?> 
	font-size: <? echo $prfarr["websitetopnvfontsize"]; ?>px;
	<? } ?>
	<? if($prfarr["websitetopnvfontcolor"]!="") { ?> 
	color: <? echo $prfarr["websitetopnvfontcolor"]; ?>;
	<? } ?>
	
	<? if(strpos($prfarr["websitetopnvfontstyle"],"Bold")!==false) { ?>
	font-weight:bold;
	<? } else { ?>
	font-weight:normal;
	<? } ?>
	
	 <? if(strpos($prfarr["websitetopnvfontstyle"],"Italic")!==false) { ?>
	font-style:italic;
	<? } else { ?>
	font-style:normal;
	<? } ?>
	
	<? if(strpos($prfarr["websitetopnvfontstyle"],"Underline")!==false) { ?>
	text-decoration:underline;
	<? } else { ?>
	text-decoration:none;
	<? } ?>
	text-align:<? echo $prfarr["websitenvfontalign"]; ?>
    cursor: pointer;
}
.topmenu:active, .topmenu:link, .topmenu:visited
{
	height:auto;
}
.topmenuselected
{
	width:<? echo $prfarr["websitetopnvwidth"]; ?>px;
	height:<? echo $prfarr["websitetopheight"]; ?>px;
    <? if($prfarr["websitetopnvfontalign"]!="") { ?> 
	text-align:<? echo $prfarr["websitetopnvfontalign"]; ?>;
	<? } ?>
	<? if($prfarr["websitetopnvrbgvalue"]!="" && fileexists($imagepath.$prfarr["websitetopnvrbgvalue"])) { ?>
	background-image:url(<? echo $imagepath.$prfarr["websitetopnvrbgvalue"]; ?>);
	<? } ?>
	<? if($prfarr["websitetopnvfonttype"]!="") { ?> 
	font-family: <? echo $prfarr["websitetopnvfonttype"]; ?>;
	<? } ?>
	<? if($prfarr["websitetopnvfontsize"]!="") { ?> 
	font-size: <? echo $prfarr["websitetopnvfontsize"]; ?>px;
	<? } ?>
	<? if($prfarr["websitetopnvrfontcolor"]!="") { ?> 
	color: <? echo $prfarr["websitetopnvrfontcolor"]; ?>;
	<? } ?>
	
	<? if(strpos($prfarr["websitetopnvrfontstyle"],"Bold")!==false) { ?>
	font-weight:bold;
	<? } else { ?>
	font-weight:normal;
	<? } ?>
	
	 <? if(strpos($prfarr["websitetopnvrfontstyle"],"Italic")!==false) { ?>
	font-style:italic;
	<? } else { ?>
	font-style:normal;
	<? } ?>
	
	<? if(strpos($prfarr["websitetopnvrfontstyle"],"Underline")!==false) { ?>
	text-decoration:underline;
	<? } else { ?>
	text-decoration:none;
	<? } ?>
	
}
.address
{
    <? if($prfarr["websitenvfontalign"]!="") { ?> 
        text-align:<? echo $prfarr["websitenvfontalign"]; ?>;
	<? } ?>
    height:22px;
	padding-left:30px;
}
/* .menulink:active, .menulink:link, .menulink:visited, .menulink  
{
	<? if($prfarr["websitenvfonttype"]!="") { ?> 
	font-family: <? echo $prfarr["websitenvfonttype"]; ?>;
	<? } ?>
	<? if($prfarr["websitenvfontsize"]!="") { ?> 
	font-size: <? echo $prfarr["websitenvfontsize"]; ?>;
	<? } ?>
	<? if($prfarr["websitenvfontcolor"]!="") { ?> 
	color: <? echo $prfarr["websitenvfontcolor"]; ?>;
	<? } ?>
	<? if(strpos($prfarr["websitenvfontstyle"],"Bold")!==false) { ?>
	font-weight:bold;
	<? } ?>
	 <? if(strpos($prfarr["websitenvfontstyle"],"Italic")!==false) { ?>
	font-style:italic;
	<? } ?>
	<? if(strpos($prfarr["websitenvfontstyle"],"Underline")!==false) { ?>
	text-decoration:underline;
	<? } else { ?>
	text-decoration:none;
	<? } ?>
	text-align:left;
}
.menulink:hover
{
	<? if($prfarr["websitenvfonttype"]!="") { ?> 
	font-family: <? echo $prfarr["websitenvfonttype"]; ?>;
	<? } ?>
	<? if($prfarr["websitenvfontsize"]!="") { ?> 
	font-size: <? echo $prfarr["websitenvfontsize"]; ?>;
	<? } ?>
	<? if($prfarr["websitenvrfontcolor"]!="") { ?> 
	color: <? echo $prfarr["websitenvrfontcolor"]; ?>;
	<? } ?>
	<? if(strpos($prfarr["websitenvrfontstyle"],"Bold")!==false) { ?>
	font-weight:bold;
	<? } ?>
	 <? if(strpos($prfarr["websitenvrfontstyle"],"Italic")!==false) { ?>
	font-style:italic;
	<? } ?>
	<? if(strpos($prfarr["websitenvrfontstyle"],"Underline")!==false) { ?>
	text-decoration:underline;
	<? } else { ?>
	text-decoration:none;
	<? } ?>
	text-align:left;
} */

.submenu {
    border-collapse: collapse;
    cursor: pointer;
    display:none;
}

.submenu_item, .submenu_item a:link, .submenu_item a:active, .submenu_item a:visited{
    <? if($prfarr["websitesubnvfontalign"]!="") { ?> 
	text-align:<? echo $prfarr["websitesubnvfontalign"]; ?>;
	<? } ?>
 	<? if($prfarr["websitesubnvbgvalue"]!="" && fileexists($imagepath.$prfarr["websitesubnvbgvalue"])) { ?>
	background-image:url(<? echo $imagepath.$prfarr["websitesubnvbgvalue"]; ?>);
	<? } ?>
	<? if($prfarr["websitesubnvfonttype"]!="") { ?> 
	font-family: <? echo $prfarr["websitesubnvfonttype"]; ?>;
	<? } ?>
	<? if($prfarr["websitesubnvfontsize"]!="") { ?> 
	font-size: <? echo $prfarr["websitesubnvfontsize"]; ?>px;
	<? } ?>
	<? if($prfarr["websitesubnvfontcolor"]!="") { ?> 
	color: <? echo $prfarr["websitesubnvfontcolor"]; ?>;
	<? } ?>
	<? if(strpos($prfarr["websitesubnvfontstyle"],"Bold")!==false) { ?>
	font-weight:bold;
	<? } ?>
	 <? if(strpos($prfarr["websitesubnvfontstyle"],"Italic")!==false) { ?>
	font-style:italic;
	<? } ?>
	<? if(strpos($prfarr["websitesubnvfontstyle"],"Underline")!==false) { ?>
	text-decoration:underline;
	<? } else { ?>
	text-decoration:none;
	<? } ?>
	border-collapse: collapse;
    cursor: pointer;
    height:30px;
    line-height: 30px;
	/*margin:0 10px;*/
    <?php if($prfarr["websitesubnvfontalign"]=="left") { ?>
    text-indent:25px;
    <? } ?>
}
.submenu_item_selected , .submenu_item_selected a:link, .submenu_item_selected a:active, .submenu_item_selected a:visited {
	<? if($prfarr["websitesubnvrbgvalue"]!="" && fileexists($imagepath.$prfarr["websitesubnvrbgvalue"])) { ?>
	background-image:url(<? echo $imagepath.$prfarr["websitesubnvrbgvalue"]; ?>);
	<? } ?>
	<? if($prfarr["websitesubnvrfonttype"]!="") { ?> 
	font-family: <? echo $prfarr["websitesubnvfonttype"]; ?>;
	<? } ?>
	<? if($prfarr["websitesubnvrfontsize"]!="") { ?> 
	font-size: <? echo $prfarr["websitesubnvrfontsize"]; ?>px;
	<? } ?>
	<? if($prfarr["websitesubnvrfontcolor"]!="") { ?> 
	color: <? echo $prfarr["websitesubnvrfontcolor"]; ?>;
	<? } ?>
	<? if(strpos($prfarr["websitesubnvrfontstyle"],"Bold")!==false) { ?>
	font-weight:bold;
	<? } ?>
	 <? if(strpos($prfarr["websitesubnvrfontstyle"],"Italic")!==false) { ?>
	font-style:italic;
	<? } ?>
	<? if(strpos($prfarr["websitesubnvrfontstyle"],"Underline")!==false) { ?>
	text-decoration:underline;
	<? } else { ?>
	text-decoration:none;
	<? } ?>
    <?php if($prfarr["websitesubnvfontalign"]=="left") { ?>
    text-indent:25px;
    <? } ?>
    border-collapse: collapse;
    cursor: pointer;
}
.submenu_item:hover, .submenu_item:hover a:link, .submenu_item:hover a:active, .submenu_item:hover a:visited {
	<? if($prfarr["websitesubnvrbgvalue"]!="" && fileexists($imagepath.$prfarr["websitesubnvrbgvalue"])) { ?>
	background-image:url(<? echo $imagepath.$prfarr["websitesubnvrbgvalue"]; ?>);
	<? } ?>
	<? if($prfarr["websitesubnvrfonttype"]!="") { ?> 
	font-family: <? echo $prfarr["websitesubnvfonttype"]; ?>;
	<? } ?>
	<? if($prfarr["websitesubnvrfontsize"]!="") { ?> 
	font-size: <? echo $prfarr["websitesubnvrfontsize"]; ?>px;
	<? } ?>
	<? if($prfarr["websitesubnvrfontcolor"]!="") { ?> 
	color: <? echo $prfarr["websitesubnvrfontcolor"]; ?>;
	<? } ?>
	<? if(strpos($prfarr["websitesubnvrfontstyle"],"Bold")!==false) { ?>
	font-weight:bold;
	<? } ?>
	 <? if(strpos($prfarr["websitesubnvrfontstyle"],"Italic")!==false) { ?>
	font-style:italic;
	<? } ?>
	<? if(strpos($prfarr["websitesubnvrfontstyle"],"Underline")!==false) { ?>
	text-decoration:underline;
	<? } else { ?>
	text-decoration:none;
	<? } ?>
    border-collapse: collapse;
    cursor: pointer;
}
#webgallery
{
	height: <? echo $prfarr["websitegalleryheight"]; ?>px;
	overflow:hidden;
}
.yang_gallery {
    position: relative;
}
.pagination {
    position: absolute;
    bottom:20px;
    text-align:center;
}
.pages {
    display:inline-block;
    border:1px solid #222;
    width:30px;
    height:30px;
    line-height:30px;
    margin:0 5px;
    font-weight:bold;

}
.current_page {
    background-color:#444;
    color: white;
}
.gallery_next, .gallery_prev {
	line-height:30px;
	margin: 5px 10px;
	cursor: pointer;
}
.action_button {
	cursor: pointer;
}
/*****************/
.spacer
{
	height:<? echo $prfarr["websitenvseparator"]; ?>px;
}
.topmenuspacer
{
	height:<? echo $prfarr["websitetopnvseperatorwidth"]; ?>px;
}
.errmsg
{
	color:#FF0000;
}

#slideshow {
    position:relative;
    height:580px;
	}

#slideshow IMG {
	vertical-align:middle;
    position:absolute;
	top:0px;
	left:5px;
    z-index:8;
    opacity:0.0;
	visibility:hidden;
}

#slideshow IMG.active {
	z-index:10;
    opacity:1.0;
	visibility:visible;
}

#slideshow IMG.last-active {
	z-index:9;
	visibility:hidden;
}
 #galleria{height:570px;width:<?=(1024-20-$prfarr['websitelpwidth']-$prfarr['websitelpwidth'])?>px;margin:0px auto;}
.music
{	
	width:<?=$prfarr['musicwidth']?>px;
	position:absolute;
	border:0px solid <?=$prfarr['musicfontcolor']?> ;
	background-color:<? if ($prfarr['musictransparent']=="yes") echo "transparent;";
else echo $prfarr['musicbgcolor']?>; 
	<?
		$musicmargin_center=(int)((1024-$prfarr['musicwidth'])/2);
		$musicmargin_right=(int)(1024-$prfarr['musicwidth']);
		if ($prfarr['musicalign']=='left') 		echo '';
		else if ($prfarr['musicalign']=='center') 	echo 'margin-left:'.$musicmargin_center.'px;'; 
		else if ($prfarr['musicalign']=='right') 	echo 'margin-left:'.$musicmargin_right.'px;'; 
	?>
	
}
.dmb_logo {
    height: 45px;
    position: absolute;
    right: 5px;
}
 </style>
<script language="javascript" src="includes/CalendarControl.js"></script>
<link type='text/css' rel='stylesheet' href='includes/CalendarControl.css' />
<link rel="stylesheet" href="js/thickbox.css" />
<script language="javascript">
var pause = 10000;
var galleryRotateTimer = 0;
var btnstr= {"bindings": [<? echo json_encode($btns); ?>]};
var redlink = {"bindings": [<? echo json_encode($redrlink); ?>]};
function showSubmenu(menuid) {
    submenu = $("#submenu"+menuid);
   	$(".submenu").each(function() {
        $(this).hide();
    });
    $(submenu).show();
}
$(document).ready(function() {
	var hbutton="#button" + "<?=$homepage_button["menu_id"]?>";
	if($(hbutton).length>0)
	{
			if($(hbutton).hasClass("menu"))
			{
				$(hbutton).removeClass("menu");
				$(hbutton).addClass("menuselected");
			}	
			if($(hbutton).hasClass("topmenu"))
			{
				$(hbutton).removeClass("topmenu");
				$(hbutton).addClass("topmenuselected");
			}	
	}
	
	
	/*var parentid="<?$homepage_button["parent_id"]?>";
	if(parentid!="0")
	{
		$("#button" + parentid).trigger('click');
		$(hbutton).addClass("submenu_item_selected");
	}*/
	//new code start to highlight subbutton
	var parentid="<?=$homepage_button["parent_id"]?>";
	if(parentid!="0")
	{
		$(hbutton).addClass("submenu_item_selected");
		
		var parent_button=$("#button" + parentid);
		parent_button.trigger('click');
		
		/* -- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---  -- */	
		/* -- this is to highlight parent button as well when children is selected -- */
			if(parent_button.hasClass("menu"))
			{
				parent_button.removeClass("menu");
				parent_button.addClass("menuselected");
			}	
			if(parent_button.hasClass("topmenu"))
			{
				parent_button.removeClass("topmenu");
				parent_button.addClass("topmenuselected");
			}
		/* -- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---  -- */	
	}
	
	/* -- this is to check and then show submenu when parent has submenu -- */
		else if ($("#submenu<?=$homepage_button["menu_id"]?>").length>0)
		{
			$("#submenu<?=$homepage_button["menu_id"]?>").show();	
		}
		//new code end
	/* -- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---  -- */	
	
	$(document).on('click','.action_button', function() {
		//alert($(this).attr('function'));
		
		//pgnm=$(this).html();
		
		pgid=$(this).attr('buttonid');
		pgnm=btnstr.bindings[0][pgid];
		gallerynm=$(this).attr('pagenumber');
		func=$(this).attr('function');
		
		//+ '&pgid=' + pgid
		
		/* pglink='<? echo "http://".$_SERVER['HTTP_HOST']."/".HOST_FOLDER.substr($_SERVER['SCRIPT_NAME'],strrpos($_SERVER['SCRIPT_NAME'],"/")+1)?>?pg=' + pgnm.toLowerCase(); */
		pglinkurl='<? echo $pglink ?>' +encodeURIComponent(encodeURIComponent(pgnm.toLowerCase()));
		
		if(typeof(gallerynm)!='undefined')  pglinkurl=pglinkurl + "&page=" + gallerynm;
		if(func!="RedirectLink")
		{
		 	go_url(pglinkurl);
		 }
		 else
		 {
		 	top.location=redlink.bindings[0][pgid];
		 }
		
	});
	/* $(".pagination .action_button").live('click', function(){
		pglink='<? echo "http://".$_SERVER['HTTP_HOST']."/".HOST_FOLDER.substr($_SERVER['SCRIPT_NAME'],strrpos($_SERVER['SCRIPT_NAME'],"/")+1)?>?pg=<?=$pagename?>&pgid=<?=$_GET["pgid"]?>' ;
		alert(pglink);
		go_url(pglink);
		return;
	}); */
		
		$(".menu").click(function() {
        $(".submenu").each(function() {
            $(this).hide();
        });
        var menuId = $(this).attr("id");
        menuId = menuId.replace("button", "");
        $("#submenu"+menuId).show();

        if($(this).is('.unclickable')) {
            return;
        }
        
		$(".submenu_item_selected").each(function() {
            $(this).removeClass("submenu_item_selected");
         });
         $(".menuselected").each(function() {
             	if(!$(this).hasClass("nochange")) {
				$(this).removeClass("menuselected");
            	$(this).addClass("menu");
				}
		   }); 
		
		$(this).removeClass("menu");
        $(this).addClass("menuselected");
		
		$(".topmenuselected").each(function() {
             	if(!$(this).hasClass("nochange")) {
				$(this).removeClass("topmenuselected");
            	$(this).addClass("topmenu");
				}
		   });
		   
	});
	 
	 $(".topmenu").click(function() {
	 		$(".submenu").each(function() {
            $(this).hide();
        	});
			var menuId = $(this).attr("id");
		    menuId = menuId.replace("button", "");
       		$("#submenu"+menuId).show();
			$(".submenu_item_selected").each(function() {
            $(this).removeClass("submenu_item_selected");
        	 });
			 
			$(".menuselected").each(function() {
				$(this).removeClass("menuselected");
				$(this).addClass("menu");
			});
			
			$(".topmenuselected").each(function() {
             	if(!$(this).hasClass("nochange")) {
				$(this).removeClass("topmenuselected");
            	$(this).addClass("topmenu");
				}
		   });
		  $(this).removeClass("topmenu");
          $(this).addClass("topmenuselected");
	 });
	

	$(".submenu_item").click(function() {
        if($(this).is('.unclickable')) {
            return;
        }
	   $(".submenu_item_selected").each(function() {
			$(this).removeClass("submenu_item_selected");
		});
	   $(this).addClass("submenu_item_selected");
	   resetFocus();
   });
	$(document).on('click','.gallery_next', function() {
		nextImage();
		clearTimeout(galleryRotateTimer);
		galleryRotateTimer = setTimeout("rotateGallery(pause)", pause);
   });
   $(document).on('click','.gallery_prev', function() {
		prevImage();
		clearTimeout(galleryRotateTimer);
		galleryRotateTimer = setTimeout("rotateGallery(pause)", pause);
   });
   $(".web_header").click(function() {
		pglink="<? echo "http://".$_SERVER['HTTP_HOST']."/".HOST_FOLDER.substr($_SERVER['SCRIPT_NAME'],strrpos($_SERVER['SCRIPT_NAME'],"/")+1)?>";
		go_url(pglink);
	});
