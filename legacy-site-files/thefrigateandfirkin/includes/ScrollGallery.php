<?php

class ScrollGallery {

    private $default_settings = array(
        "id" => "scroll_gallery",
		"imageroot" => "",
		"width" => 600,
		"height" =>550
    );
    private $settings;
    private $code;
    private $files = array();
	
    function __construct() {
        if (func_num_args() == 1) {
            $files = func_get_arg(0);
            $this->createImageGallery($files);
        } else if (func_num_args() == 2) {
            $files = func_get_arg(0);
            $settings = func_get_arg(1);
            $this->createImageGallery($files, $settings);
        }
    }
	
	function createImageGallery($files, $settings = false) {
		//print_r($files);
		if($settings == false) {
			$this->settings = $this->default_settings;
		} else {
			$this->settings = array_merge($this->default_settings, $settings);
		}
		$this->code = "<center><div id='{$this->settings['id']}' style='max-height:{$this->settings['height']}px;max-width:{$this->settings['width']}px;overflow:hidden;'>\n";
		foreach($files as $file) {
			$div = 	"<div id='img{$file['imageid']}' class='scrollableGallery' style='display:none;height:{$this->settings['height']}px;width:{$this->settings['width']}px'><img class='gallery_next' src='{$file['name']}' style='max-height:{$this->settings['height']}px;max-width:{$this->settings['width']}px;margin:0 auto;' /></div>\n";
			$this->code .= $div;
		}
		$this->code .= "</div></center>\n";
	}	
	
	public function getCode() {
		return $this->code;
	}
}
/*
$files = array(
			"../images/test/1.jpg",
			"../images/test/2.jpg",
			"../images/test/3.jpg",
			"../images/test/4.jpg",
			"../images/test/5.jpg",
			);
$g = new ScrollGallerY($files);
echo $g->getCode();
*/
?>
