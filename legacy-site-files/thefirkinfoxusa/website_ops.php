<?
	include("./includes/client.php");
	require("./includes/ImageGallery.php");
	require("./includes/ScrollGallery.php");

	$req = array_merge($_GET, $_POST, $_FILES);
	$datatype = array_key_exists("datatype", $req) ? $req["datatype"] : "";
	if ($datatype == "scrollGallery" && !empty($req['buttonid'])) 
	{
    	$button_id = $req['buttonid'];
    	
		$files = array();
   		
		$galleryId = $button_id;
				
				$request = array(
    		    'id' => CLIENT_API_ID,
		        'api_secret' => CLIENT_API_KEY,
       			'request' => array(
	            'name' => "getgalleryimages",
				'arguments' => array(
						'galleryid' =>$galleryId,
					)	
            	),
     		);
		$res = curl_post("http://www.digitalmarketingbox.com/api/service.php", array("request"=>json_encode($request)));
		$galleryarr = json_decode($res, true);	
		foreach($galleryarr as $row)
			{
				  $file['name'] = $row['image_path'];
       			  $file['galleryid'] = $galleryId;
        	 	  $file['imageid'] = $row['image_id'];
        		  $files[] = $file;	
			}
	
	$settings=array( "width"=>$req['width']);
    
	$scrollGallery = new ScrollGallery($files,$settings);
    echo $scrollGallery->getCode();
    if (sizeof($files) > 1) {
        echo "<a class='gallery_prev'>Previous</a>";
        echo "<a class='gallery_next'>Next</a>";
    }
}
else if($datatype =="lightboxFullGallery" && !empty($req['buttonid']))
{
		$button_id = $req['buttonid'];
    	$files = array();
   		
		$galleryId = $button_id;
				
				$request = array(
    		    'id' => CLIENT_API_ID,
		        'api_secret' => CLIENT_API_KEY,
       			'request' => array(
	            'name' => "getgalleryimages",
				'arguments' => array(
						'galleryid' =>$galleryId,
					)	
            	),
     		);
		$res = curl_post("http://www.digitalmarketingbox.com/api/service.php", array("request"=>json_encode($request)));
		$galleryarr = json_decode($res, true);	
		
		$thumbnailtop=".galleria-thumbnails-container {top:10px;}
		.galleria-stage{ top:60px;bottom:10px;}";
		$thumbnailbot=".galleria-thumbnails-container {top:auto;bottom:0px;}
		.galleria-stage{ top:10px;bottom:60px;}";
		?>
        <style>
		#galleria
		{
			<? if ($req['lightboxheight']!=0) echo "height:{$req['lightboxheight']}px;"; else echo "height:570px;";
			if ($req['lightboxwidth']!=0) echo "width:{$req['lightboxwidth']}px;"; else echo "width:".$req['width']."px;";
			?>
		}
		<? if ($req['lightboxposition']=='top')  echo $thumbnailtop;
		else echo $thumbnailbot;
		?>
        </style>
        
		<?
		if(count($galleryarr)>0)
		{
			echo "<div id=\"galleria\" style=\"display:none;z-index:1;\">";
			foreach($galleryarr as $row)
			{
				echo "<img src=\"".$row['image_path']."\">";
			}
			echo "</div><div id=\"popupoverlay\" class=\"overlay\"></div>";
		}
}
else if($datatype =="sendenquiry")
{
	echo "<p>&nbsp;</p>";
	$req["profilename"]=PROFILE;
	$res = curl_post(WEB_DATA_URL."profile_ops.php",$req);
	echo $res;	
} 
else if($datatype =="sendSubscribeNewsletterForm")
{
	echo "<p>&nbsp;</p>";
	$req["profileid"]=PROFILEID;
	$res = curl_post(WEB_DATA_URL."profile_ops.php",$req);
	echo $res;	
} 
else if($datatype =="sendPrivatePartyForm")
{
	echo "<p>&nbsp;</p>";
	$req["profileid"]=PROFILEID;
	$res = curl_post(WEB_DATA_URL."profile_ops.php",$req);
	echo $res;	
}
else if ($datatype =="otherforms")
{
	
	 $filearr=$_FILES['F1'];
	/*for($i=0; $i < count($filearr['name']); $i++) 
		{
		    // if the upload succeded, the file will exist
		    if (file_exists($filearr['tmp_name'][$i]))
		    {
		    	if(is_uploaded_file($filearr['tmp_name'][$i]))
		    	{
					 $req["F1[]"] = '@' . $filearr['tmp_name'][$i] . ';filename=' . $file['name'][$i] . ';type=' . $file['type'][$i];
				}
			}		
		}
	*/
	
	$count=0;
	for($i=0;$i<count($_FILES['F1']['name']);$i++)
	{
	//foreach ($_FILES['F1'] as $param => $file) {
	if 	(trim($_FILES['F1']['tmp_name'][$i])!=""){
     $req["F1[{$count}]"] = '@' . $_FILES['F1']['tmp_name'][$i] . ';filename=' . $_FILES['F1']['name'][$i] . ';type=' . $_FILES['F1']['type'][$i];
	 //echo   $i .'----- @' . $_FILES['F1']['tmp_name'][$i] . ';filename=' . $_FILES['F1']['name'][$i] . ';type=' . $_FILES['F1']['type'][$i];
	  $count++;
	} 
	}
 			
	echo "<p>&nbsp;</p>";
	
	$req["profileid"]=PROFILEID;
	$req["datatype"]="otherforms";
	$res = curl_post(WEB_DATA_URL."profile_ops.php",$req);
	echo $res;		
}
 else
 {
 	echo "Invalid Request";
 }
 
?>
