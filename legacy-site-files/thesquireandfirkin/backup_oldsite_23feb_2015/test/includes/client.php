	<?php
	define("PROFILE","squire_new");
	define("PROFILEID","100291");
    
	define("CLIENT_API_ID", "e6c5952acbf90b018c7afbcea2a8f29f");
	define("CLIENT_API_KEY", "686c21430dafae6fef9c0734e9d50b98");
	define("WEB_DATA_URL", "http://www.digitalmarketingbox.com/webtool/");
	//define("HOST_FOLDER", "tosca/");
  	define("HOST_FOLDER","thesquireandfirkin/test/");
	define("CLIENT_URL","http://".$_SERVER['HTTP_HOST']."/".HOST_FOLDER);
	define("API_LINK", "http://www.digitalmarketingbox.com/api/service.php");
	//ini_set("DISPLAY_ERRORS","ON");
	
	function curl_post($addr, $postArray) {
    $ch = curl_init();
	
	curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
	curl_setopt ($ch, CURLOPT_HEADER, 0);
	curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
	
    curl_setopt($ch, CURLOPT_URL, $addr);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postArray);
    // Download the given URL, and return output
    $output = curl_exec($ch) or die(curl_error($ch));
    // Close the cURL resource, and free system resources
    curl_close($ch);
	
	if(is_int($output)) {
    	die("Errors: " . curl_errno($ch) . " : " . curl_error($ch));
	}
    return $output;
}
	function setcase($word,$case)
	{
		if($case=='ucfirst')
		{
	 	return ucfirst($word);
		}
		else if ($case=='strtoupper') 
		{
			return strtoupper($word);
	