	<?php
	define("PROFILE","squireandfirkin");
	define("PROFILEID","100038");
    
	define("CLIENT_API_ID", "e6c5952acbf90b018c7afbcea2a8f29f");
	define("CLIENT_API_KEY", "686c21430dafae6fef9c0734e9d50b98");
	define("WEB_DATA_URL", "http://www.digitalmarketingbox.com/webtool/");
    //define("HOST_FOLDER", "tosca/");
	define("HOST_FOLDER", "thesquireandfirkin/");
	define("CLIENT_URL","http://".$_SERVER['HTTP_HOST']."/".HOST_FOLDER);
	define("API_LINK", "http://www.digitalmarketingbox.com/api/service.php");
	//ini_set("DISPLAY_ERRORS","ON");
	
	function curl_post($addr, $postArray) {
    $ch = curl_init();
	
	curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
	curl_setopt ($ch, CURLOPT_HEADER, 0);
	curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
	
    curl_setopt($ch, CURLOPT_URL, $addr);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postArray);
    // Download the given URL, and return output
    $output = curl_exec($ch) or die(curl_error($ch));
    // Close the cURL resource, and free system resources
    curl_close($ch);
	
	if(is_int($output)) {
    	die("Errors: " . curl_errno($ch) . " : " . curl_error($ch));
	}
    return $output;
}
	function setcase($word,$case)
	{
		if($case=='ucfirst')
		{
	 	return ucfirst($word);
		}
		else if ($case=='strtoupper') 
		{
			return strtoupper($word);
		}
		else if ($case=='strtolower')
		{
	 	return strtolower($word);
		}
		else 
		{
			return $word;
		}	
	}
	function array_sort($array, $on, $order=SORT_ASC)
{
    $new_array = array();
    $sortable_array = array();

    if (count($array) > 0) {
        foreach ($array as $k => $v) {
            if (is_array($v)) {
                foreach ($v as $k2 => $v2) {
                    if ($k2 == $on) {
                        $sortable_array[$k] = $v2;
                    }
                }
            } else {
                $sortable_array[$k] = $v;
            }
        }

        switch ($order) {
            case SORT_ASC:
                asort($sortable_array);
            break;
            case SORT_DESC:
                arsort($sortable_array);
            break;
        }

        foreach ($sortable_array as $k => $v) {
            $new_array[$k] = $array[$k];
        }
    }

    return $new_array;
}
	function recursiveArraySearch($haystack, $needle, $index = null)
	{
    	$aIt     = new RecursiveArrayIterator($haystack);
    	$it    = new RecursiveIteratorIterator($aIt);
   
    	while($it->valid())
    	{       
        	if (((isset($index) AND ($it->key() == $index)) OR (!isset($index))) AND ($it->current() == $needle)) {
           	 return $aIt->key();
    	   	}
         $it->next();
   	 	}
     return false;
	} 
	function fileexists($imageurl)
	{
		/* if (fopen(WEB_DATA_URL.$imageurl, "r")) 
		{
			return true;
		}
		else
		{
			return false;
		} */
		return true;
	}
	function filename_extension($filename)
{
    $pos = strrpos($filename, '.');
    if($pos===false) {
        return false;
    } else {
        return substr($filename, $pos+1);
    }
}
function getExtCssString($fontname)
{
		$fontname = str_replace(" ", "", $fontname);
		return "<link rel='stylesheet' href='./fonts/{$fontname}/stylesheet.css' />\n";
}
/*
	if ( !function_exists('json_decode') ){
	function json_encode( $data ) {           
    if( is_array($data) || is_object($data) ) {
        $islist = is_array($data) && ( empty($data) || array_keys($data) === range(0,count($data)-1) );
       
        if( $islist ) {
            $json = '[' . implode(',', array_map('json_encode', $data) ) . ']';
        } else {
            $items = Array();
            foreach( $data as $key => $value ) {
                $items[] = json_encode("$key") . ':' . json_encode($value);
            }
            $json = '{' . implode(',', $items) . '}';
        }
    } elseif( is_string($data) ) {
        # Escape non-printable or Non-ASCII characters.
        # I also put the \\ character first, as suggested in comments on the 'addclashes' page.
        $string = '"' . addcslashes($data, "\\\"\n\r\t/" . chr(8) . chr(12)) . '"';
        $json    = '';
        $len 