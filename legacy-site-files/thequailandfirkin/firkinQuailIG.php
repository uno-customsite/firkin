<?php
    //INSTAGRAM
    
    //\(([^()]*+|(?R))*\)

    // Gets our data
    function fetchData($url){
         $ch = curl_init();
         curl_setopt($ch, CURLOPT_URL, $url);
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
         curl_setopt($ch, CURLOPT_TIMEOUT, 20);
         $result = curl_exec($ch);
         curl_close($ch); 
         return $result;
    }

    // Pulls and parses data.
    $result = fetchData("https://api.instagram.com/v1/users/2286228948/media/recent?client_id=9d42b5169c254bf2a61334faee1aa7ff");
    $result = json_decode($result);
?>
<!DOCTYPE HTML>

<html>
    <head>
        <meta name="keywords" content="Keywords Here" />
        <meta name="description" content="Description Here" />
        <meta name="author" content="Evan Paradis" />
        <meta charset="utf-8">

        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <title>Firkin Harbour Instagram</title>

        <style>
            body, html {
                margin: 0;
                padding: 0;
                width: 100%;
                min-height: 100%;
                height: auto;
            }
            .ig-wrap * {
                margin: 0;
                padding: 0;
                -webkit-box-sizing: border-box;
                -moz-box-sizing: border-box;
                box-sizing: border-box;
            }
            .ig-wrap ul {
                list-style-type: none;
            }
            .ig-wrap ul li {
                float: left;
                width: 20%;
                padding: 3px;
            }
            .ig-wrap ul li img {
                display: block;
                width: 100%;
                padding: 5px;
                background-color: #f2f2f2;
                border-radius: 15px;
            }
        </style>
    </head>
    <body>
        <div class="ig-wrap">
            <ul>
                <?php
                    $i = 0;
                    foreach ($result->data as $post) {
                        if ($i == 5) break;
                        $i++;
                ?>
                <!-- Renders images. @Options (thumbnail,low_resolution, high_resolution) -->
                <li>
                    <a href="http://instagram.com/quailandfirkin" target="_blank" data-gallery><img src="<?php echo $post->images->low_resolution->url; ?>" class="ig-img"></a>
                </li>
                <?php
                    }
                ?>
                <div class="clear"></div>
            </ul>
        </div>
    </body>
</html>