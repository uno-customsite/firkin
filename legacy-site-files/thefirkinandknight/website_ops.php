<?
	include("./includes/client.php");
	require("./includes/ImageGallery.php");
	require("./includes/ScrollGallery.php");

	$req = array_merge($_GET, $_POST, $_FILES);
	$datatype = array_key_exists("datatype", $req) ? $req["datatype"] : "";
	if ($datatype == "scrollGallery" && !empty($req['buttonid'])) 
	{
    	$button_id = $req['buttonid'];
    	
		$files = array();
   		
		$galleryId = $button_id;
				
				$request = array(
    		    'id' => CLIENT_API_ID,
		        'api_secret' => CLIENT_API_KEY,
       			'request' => array(
	            'name' => "getgalleryimages",
				'arguments' => array(
						'galleryid' =>$galleryId,
					)	
            	),
     		);
		$res = curl_post("http://www.digitalmarketingbox.com/api/service.php", array("request"=>json_encode($request)));
		$galleryarr = json_decode($res, true);	
		foreach($galleryarr as $row)
			{
				  $file['name'] = $row['image_path'];
       			  $file['galleryid'] = $galleryId;
        	 	  $file['imageid'] = $row['image_id'];
        		  $files[] = $file;	
			}
	
	$settings=array( "width"=>$req['width']);
    
	$scrollGallery = new ScrollGallery($files,$settings);
    echo $scrollGallery->getCode();
    if (sizeof($files) > 1) {
        echo "<a class='gallery_prev'>Previous</a>";
        echo "<a class='gallery_next'>Next</a>";
    }
}
else if($datatype =="lightboxFullGallery" && !empty($req['buttonid']))
{
		$bu