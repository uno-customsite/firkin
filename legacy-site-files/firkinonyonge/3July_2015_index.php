<?php
	//set_time_limit(60);
	include("./includes/client.php");
	
	$request = array(
        'id' => CLIENT_API_ID,
        'api_secret' => CLIENT_API_KEY,
        'request' => array(
            'name' => "getWebProfileInfo",
			'arguments' => array(
					'profile' => PROFILE
				)	
            ),
     );
	 
	$res = curl_post(API_LINK, array("request"=>json_encode($request)));
	
	$res=utf8_encode($res);
		
	$prfarr = json_decode($res,  $assoc = true);
	/*print_r($prfarr);
	exit();*/
			
	@$pagename=$_GET["pg"];
	if(strpos($pagename,"/")!==false)
	{
		if(strpos($pagename,"/")==strlen($pagename)-1) $pagename=substr($pagename,0,strlen($pagename)-1);  
	}	
	
	$imagepath="webdata/".strtolower(PROFILEID)."/";	
	$contact_address=$prfarr["weblocation"].", ".$prfarr["webcity"].", ".$prfarr["webstate"].", ".$prfarr["webcountry"].", ".$prfarr["webpostalcode"];
	$contenttop=$prfarr["websitenvtop"];
	
	switch($prfarr["templateid_fk"])
	{
		case 2: 
				$csslink="webcss/variable_fontstyle.css";
				break;
		default: 
				$csslink="webcss/fixed_fontstyle.css";	
				break;
	}
	
	$main_menus = array();
	$sub_menus = array();
	$homepage_button = array();
	$btns=array();
	$redrlink=array();
	$spchar=array(" ","/","_");
	foreach($prfarr['website_buttons'] as $menu) {
		$btns[$menu['menu_id']]=str_replace($spchar,"_",$menu['menu_name']);
		//echo $menu['menu_name']." ".urlencode($menu['menu_name'])."<br>";
		if($menu["function"]=="RedirectLink") $redrlink[$menu['menu_id']]=$menu["content"];
		
		if($menu['parent_id']==0) {
			$main_menus[] = $menu;
			if($menu['is_homepage'] == 1 && count($homepage_button)==0) {
				$homepage_button = $menu;
			}
		} else {
			$sub_menus[] = $menu;
		}
			//echo strtoupper(str_replace($spchar,"",$menu['menu_name']))." ".strtoupper(str_replace($spchar,"",$pagename))."<br>";
			//if(strtoupper($menu['menu_name'])==stripslashes(strtoupper(str_replace($spchar," ",$pagename)))) 
			if(strtoupper(str_replace($spchar," ",$menu['menu_name']))==stripslashes(strtoupper(str_replace($spchar," ",$pagename)))) 
			{
				$homepage_button = $menu;
			}	
	} // for loop end here
	$main_menus=array_sort($main_menus, 'menu_order', SORT_ASC);
	$sub_menus=array_sort($sub_menus, 'menu_order', SORT_ASC);
	
	//print_r($sub_menus);
	//exit();
	
	$rediectlink=array("RedirectLink","showOpenTableReservation");
	
	if($prfarr["hasshortlink"]!=1)
	{
		$pglink="http://".$_SERVER['HTTP_HOST']."/".HOST_FOLDER.substr($_SERVER['SCRIPT_NAME'],strrpos($_SERVER['SCRIPT_NAME'],"/")+1)."?pg=";
     }
	 else
	 {
	 	$pglink="http://".$_SERVER['HTTP_HOST']."/".HOST_FOLDER;
	 }  
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<link rel="icon" href="http://www.firkinpubs.com/old/favicon.ico" type="image/x-icon"/>
<link rel="shortcut icon" href="http://www.firkinpubs.com/old/favicon.ico"/>
<?php if(strtolower($pg) == "home") { ?>
<link rel="canonical" href="http://www.firkinpubs.com/firkinonyonge/home" />
<? } ?>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="Content-Language" content="EN">
<title><?php ?><?php echo ($prfarr["webtitle"]!="")?$prfarr["webtitle"]:PROFILE; ?><?php ?></title>

<meta name="title" content="<?php echo ($prfarr["webtitle"]!="")?$prfarr["webtitle"]:PROFILE; ?>" />
<meta name="keywords" content="<?php echo ($prfarr["webkeyword"]!="")?$prfarr["webkeyword"]:PROFILE; ?>" />
<meta name="description" content="<?php echo ($prfarr["webdescription"]!="")?$prfarr["webdescription"]:PROFILE; ?>" />
<META name="expires" content="never">
<META name="charset" content="UTF-8">
<META NAME="AUTHOR" CONTENT="Digital Menubox">
<META NAME="COPYRIGHT" CONTENT="Copyright@2010 <?php echo PROFILE; ?>">
<META NAME="DISTRIBUTION" CONTENT="Global">
<META NAME="RATING" CONTENT="General">
<META NAME="ROBOTS" CONTENT="Index, Follow">
<?
 if(trim($prfarr["analyticaccount"])!="") { ?>
<?php /*?><script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', '<? echo $prfa