/* =========================================================================== */
/* DECLARE GLOBAL FUNCTIONS
/* =========================================================================== */
function getpagecontents(name){
    $("#page-section").append('<hr class="temp-placeholder" id="placeholder-for-'+name+'" style="display:none;">');
    $.ajaxSetup( { "async": true, "cache": true } );
    $.get( template_url+name+'.html', function( data ) {
        $('#placeholder-for-'+name).after(data).remove();
        $("#"+name).css("height",$(window).height()+"px");
        $("#"+name+" > div").css("height",$(window).height()+"px");
    });
}
function videoHeight(item) {
    setTimeout(function() {
        $(item).each(function() {
            var height = $(this).width() / 1.77;
            $(this).height(height+"px"); 
        });
    }, 0);
}
/* =========================================================================== */
/* WINDOW LOAD
/* =========================================================================== */

/* =========================================================================== */
/* DOCUMENT READY 
/* =========================================================================== */
$(document).ready(function() {
	videoHeight('.custom-video');

	/* -------------------------------------------------- */
	/* DECLARE VARS */
	/* -------------------------------------------------- */
	// SCROLL VARS
	var window_scroll = window.pageYOffset;
	var window_scroll_prev = window.pageYOffset;
	// NAVIGATION VARS
	var mobile_nav_open = false;
	var header_section = $('#navbar_wrap');
	var header = $('#header-main');
	var nav = $('.nav-bar');
	var nav_content = $('#nav_main');
	var nav_controls = $('#nav-main-controls');
	var mobile_nav_button = $('#nav-main-toggle');
	// HEADER VARS 
	var head = $('header');
	var hero_header = $('#header_wrap');
	var short_header = $('#header_wrap.short-head');
	var parallax_elem = $('.bg-parallax');
	// ELEM. VARS
	var html = $('html');
	var body = $('body');
	var page = $('#page-section');
	var elem_home = $('#home');

	/* -------------------------------------------------- */
	/* HERO / HEADER   */
	/* -------------------------------------------------- */
	// if ($(window).width() > 736 ) {
	// 	short_header.css({"height": '400px'});	
	// } else {
	// 	short_header.css({"height": "250px"});
	// }

	/* -------------------------------------------------- */
	/* DECLARE LOCAL FUNCTIONS */
	/* -------------------------------------------------- */
	function isMobileDisplay() {
		return nav_controls.css('display') === 'block' ? true : false;
	}
	function showMobileNav() {
		mobile_nav_open = true; // UPDATE VARS
		header.addClass('mobile-open'); // TOGGLE CLASSES

		toggleBodyFix(); // CALL FUNCTIONS
		setMobileNavDim();
    }
	function hideMobileNav(scrollFunction, scrollValue, scrollDistance) {
		mobile_nav_open = false; // UPDATE VARS
		header.removeClass('mobile-open'); // TOGGLE CLASSES

		nav_controls.css({'background-color': ''}); // RESET INLINE STLYES
		nav.css({'background-color': '', 'height' : ''});
		nav_content.css({'margin-top' : ''});

		toggleBodyFix(); // CALL FUNCTIONS

		if ( scrollFunction !== undefined && scrollValue !== undefined && scrollDistance !== undefined) {
			scrollFunction(scrollValue, scrollDistance);
		} else {
			// CALLBACK IS NOT DEFINED
		}
	}
	function resizeMobileNav() {
		if (isMobileDisplay() && mobile_nav_open) {
			setMobileNavDim();
		} else if ( !isMobileDisplay() && mobile_nav_open ) { 
			hideMobileNav();
		} 
	}
	function setMobileNavDim() {
		var offset = nav_controls.height(); // DECLARE VARS
		if ( ($(window).height() - offset) > nav_content.outerHeight() ) { // SET nav_content HEIGHT/POSITION
			var offset = nav_controls.height();
			var nav_margin = (calcWinHeight(offset) - nav_content.outerHeight()) / 2;
			
			nav.height(calcWinHeight(offset)); // SET nav HEIGHT
			nav_content.css({ 'margin-top': nav_margin});
		} else {
			nav.height(calcWinHeight(offset)); // SET nav HEIGHT
			nav_content.outerHeight(calcWinHeight(offset));
			nav_content.css({ 'height': ''});
		}
	}
	function toggleBodyFix() {
		var body_is_fixed = body.hasClass('body-fixed') ? true : false;
		if ( body_is_fixed ) {
			body.removeClass('body-fixed');
			body.css({'height': '', 'position': '', 'top': '', 'overflow': ''});
			html.scrollTop(window_scroll_prev); // Firefox
			body.scrollTop(window_scroll_prev); // Chrome, Safari
		} else {
			window_scroll_prev = window_scroll; // STORE window_scroll VALUE FOR LATER USE
			body.addClass('body-fixed');
			body.css({'height': calcWinHeight(), 'position': 'fixed', 'top': (window_scroll*-1), 'overflow': 'hidden'});
		}
	}
	function scrollToLoc(scrollValue, scrollDistance) {
		var scroll_dur = calcScrollSpeed(scrollDistance)
		$('html, body').animate({scrollTop: scrollValue}, scroll_dur);
	}
	function calcScrollDist(windowLoc, targetLoc) {
		var value;
		value = windowLoc - targetLoc;
		if (value < 0) {
			value = value*-1;
		}
		return value;
	}
	function calcScrollSpeed(dist) {
		var value;
		if ( dist <= 500 ) {
			value = 750;
		} else if ( dist >= 2000 && dist < 4500 ) {
			value = 2000;
		} else if ( dist >= 4500 ) {
			value = 3000;
		} else {
			value = dist;
		}
		return value;
	}
	function calcWinHeight(offset) {
		var rcvd_offset = ((offset !== 'undefined') && !(isNaN(offset))) ? true : false;
		var value;
		if (rcvd_offset) {
			value = ($(window).height() - offset); 
			return value;
		} else {
			value = $(window).height();
			return value;
		}
	}
	function setActive() {
		var active = window.location.href.split("/").pop().replace(".html", '');
	  	$(document).find('#nav_main > li a').each(function() {
	  		if (active == 'index') {
	  			if ($(this).attr("data-href").replace("#",'') == 'home') {
	  				$(this).parent().addClass('active');
	  			}
	  		} else if ($(this).attr("data-href").replace("#",'') == active) {
	  			$(this).parent().addClass('active');
	  		}
	  	});
	}
	setActive();
	function setPadding() {
		var paddingTop = $('.logo').width();
		var max;
	    if ($('.logo').hasClass('large')) {
	       	if ($("#page-section:before").css('zIndex') == 0) {
	       		max = 675;
	       	} else {
	       		max = 575;
	      	}
	    } else if ($('.logo').hasClass('medium')) {
	        if ($("#page-section:before").css('zIndex') == 0) {
	        	max = 495;
	        } else {
	        	max = 395;
	      	}
	    } else if ($('.logo').hasClass('small')) {
	        if ($("#page-section:before").css('zIndex') == 0) {
	        	max = 300;
	        } else {
	        	max = 250;
	        }
       	}
	    if (paddingTop >= max) {
	      	$('.logo').css({"padding-top": max + 'px'});
	    } else {
	        $('.logo').css({"padding-top": ''});
	    }
	}
		
	function eventResize() {
	 	setPadding();
		resizeMobileNav();
		// When window is resized - close button toggle
		if ($(window).width() > 600) {
			header.removeClass('mobile-open');
			if (mobile_nav_open) {
				hideMobileNav();
			} 
		}
	 	// When window is mobile width for short-head pages
		// if ($(window).width() > 736 ) {
		// 	short_header.css({"height": '400px'});
		// } else {
		// 	short_header.css({"height": "250px"});
		// }
	 	videoHeight('.custom-video');
	}

	function parallax() {
		videoHeight('.custom-video');
		window_scroll = window.pageYOffset;
		var scrolled = $(window).scrollTop();
		// Background scrolled effect
		parallax_elem.css('top', (scrolled*0.9)+'px');
		// Text scrolled opacity effect
		$('#header_wrap .logo').css('opacity','1'-(scrolled*0.005));
			$('#header_wrap .headline h1, #header_wrap .headline h2').css('opacity','1' - (scrolled * 0.0015));
		$('#header_wrap #social').css('opacity','1'-(scrolled*0.001));
		// When window is scrolled past hero section
		if (scrolled > head.height() - 72) {
		    header_section.addClass('bgnav');                
		} else {
		    header_section.removeClass('bgnav');
		}
	}

	var carousel;
	var carousel_speed;
	var carousel_length;
	var carousel_type;
	var timer;
	var active_slide;
	var previous_slide;
	function initCarousel() {
		if ($('.uno-carousel').attr("data-carousel")) {
			carousel = JSON.parse($('.uno-carousel').attr("data-carousel"));
			carousel_speed = carousel.speed * 1000;
			carousel_length = carousel.length;
			carousel_type = carousel.type;
		}

        if (carousel) {
            if (carousel_length) {
	                
	            // Do the init;
	            active_slide = 0;

	            if (carousel_speed > 0) { // $scope.websiteList[0].auto_scroll
	                if (carousel_type == 'slide') {
                    	// Make sure uno-carousel is always at least as wide as all carousel images put together
                    	$('.uno-carousel').css({"width": "100vw", "white-space": "nowrap"});
                        timer = setTimeout(function() {
                            runSlide();
                        }, carousel_speed);
                    }
                    if (carousel_type == 'fade') {
                        timer = setTimeout(function() {
                            runFade();
                        }, carousel_speed);
                    }
                }
            }
        }
        return;
    }

    function runSlide(reverse) {
        if (reverse) {
            if (active_slide == 0) {
                active_slide = carousel_length - 1;
            } else {
                active_slide -= 1;
            }
        } else {
            if (active_slide != (carousel_length - 1)) {
                active_slide += 1;
            } else {
                active_slide = 0;
            }
        }
	        
        var new_left = '-' + (active_slide * 100) + 'vw';

        $('.uno-carousel').animate({
            left: new_left
        }, 1000, function() {
           	timer = setTimeout(function() {
                runSlide();
            }, carousel_speed);
        });
	}

    function runFade(reverse) {
        previous_slide = active_slide;
        if (reverse) {
            if (active_slide == 0) {
                active_slide = carousel_length - 1;
            } else {
                active_slide -= 1;
            }
        } else {
            if (active_slide != (carousel_length - 1)) {
               	active_slide += 1;
            } else {
                active_slide = 0;
            }
        }

        $('.uno-carousel > div').each(function() {
        	$(this).removeClass('active-slide');
        	$(this).removeClass('previous-slide');
        })

        $('[data-id="slide-' + active_slide + '"]').addClass('active-slide');
        $('[data-id="slide-' + previous_slide + '"]').addClass('previous-slide');

	    // Delay to accomodate classes (time split with animation)
	    setTimeout(function() {
	        $('.active-slide').css({"opacity": "1"});

            $('.previous-slide').animate({
                opacity: 0
            }, 500, function() {
                timer = setTimeout(function() {
                    runFade();
                }, carousel_speed);
            });
        }, 500);
    }
    $('.uno-carousel-arrow-left').click(function() {
        clearTimeout(timer);

        if (carousel_type == 'slide') {
            runSlide(true);
        }
        if (carousel_type == 'fade') {
            runFade(true);
        }
    });

    $('.uno-carousel-arrow-right').click(function() {
        clearTimeout(timer);

        if (carousel_type == 'slide') {
            runSlide();
        }
        if (carousel_type == 'fade') {
            runFade();
        }
	});

	/* -------------------------------------------------- */
	/* EVENTS */
	/* -------------------------------------------------- */
	mobile_nav_button.on('click', function() {
		if (mobile_nav_open) {
			hideMobileNav();
		} else {
			showMobileNav();
		}
	});
	// Close mobile toggle when these two things happen:
	// 1. When window is clicked, hide menu
	nav.on('click', function() {
		if (mobile_nav_open) {
			hideMobileNav();
		} 
	});
	// 2. When esc key is pressed
	$(document).keyup(function(e) {
	    if (e.keyCode == 27 && mobile_nav_open) {
	    	hideMobileNav();
	    } 
	});

	// SCROLL EVENTS
	$(window).on('scroll', parallax);
	$(window).on('resize', eventResize);
	setPadding();
	initCarousel();
});