<?php

class ImageGallery {

    private $default_settings = array(
        "id" => "yang_gallery",
        "width" => 600,
        "height" => 600,
        "page_limit" => 0,
        "container_v_padding" => 0,
        "container_h_padding" => 0,
        "v_spacing" => 10,
        "h_spacing" => 10,
        "display_filename" => false,
        "auto_fit" => true, //pack the container to be the minimum size that wraps around the content
        "col_limit" => 2, //number of columns per page, this will determine how big the image will be
        "show_pagination" => true,
        'max_image_height' => 150,
        "extra_space_h" => 50,
        "htmlRoot" => "",
        "caller" => ""
    );
    private $settings;
    private $code;
    private $files = array();
	private $file_details = array();
    private $page_number = 1;
    private $num_pages;
	
    private $page_mapping = array();

    function __construct() {
        if (func_num_args() == 1) {
            $files = func_get_arg(0);
            $this->createImageGallery($files);
        } else if (func_num_args() == 2) {
            $files = func_get_arg(0);
            $settings = func_get_arg(1);
            $this->createImageGallery($files, $settings);
        }
    }

    /*
     * Displays all the items in this 
     */

    public function displayAll() {
        
    }

    public function displayPage($page_number=1) {
        if($page_number>$this->getNumberOfPages()) {
            $page_number = $this->getNumberOfPages();
        }
        $this->page_number = $page_number;
        $page_size = $this->calcPageSize();
        //echo "Page size " . $this->getNumberOfPages();

        $page_first_item = ($page_number - 1) * $page_size;
        $real_width = $this->settings['actual_width']; // - $this->settings['container_h_padding'] * $this->settings['col_limit'];
        $code = "<center><div id='{$this->settings['id']}' class='yang_gallery' style='padding:{$this->settings['container_v_padding']}px {$this->settings['container_h_padding']}px;width:{$real_width}px;height:{$this->settings['height']}px;'>\n";

        $colCount = 0;
        for ($i = $this->page_mapping[$page_number]; $i < sizeof($this->files); $i++) {
            $file = $this->files[$i];
            $max_height = ($max_height > $this->calcThumbnailHeight($file)) ? $max_height : $this->calcThumbnailHeight($file);
            if ($max_height > $this->settings['max_image_height']) {
                $max_height = $this->settings['max_image_height'];
            }
            if ($accHeight + $max_height > $this->settings['actual_height'] - $this->settings['extra_space_h']) {
                break;
            }
            if ($colCount == 0) {
                $code.= "<div style='clear:both'>\n";
            }

            $code.=$this->getThumbnailCode($file, $this->calcThumbnailWidth());
            $colCount++;
            if ($colCount == $this->settings['col_limit']) {
                $accHeight+= $max_height;
                $max_height = 0;
                $code.= "</div>\n\n\n";
                $colCount = 0;
            }
        }
        if ($this->settings['show_pagination'] === true && $this->getNumberOfPages()>1) {
            $code .= $this->createPaginationCode();
        } else {

        }
        $code.= "</div></center>\n\n\n\n";

        return $code;
    }

    public function getThumbnailCode($file, $width, $prettyPhoto=true) {
		$details = array();
		//print_r($this->file_details);
		foreach($this->file_details as $det) {
			if($det['name']==$file) {
				$details = $det;
			}
		}
        $height = $this->calcThumbnailHeight($file);
        $img = "<img src='{$file}' style='max-width:{$width}px;max-height:{$this->settings['max_image_height']}px' />";
            $code = "<a title='{$file}' imageid='{$details['imageid']}' galleryid='{$details['galleryid']}' style='cursor:pointer' rel='colorbox'>" . $img . "</a>";
        if ($prettyPhoto) {
            //$code = $img;
        }
        if ($this->settings['display_filename'] === true) {
            $picture = $file;
        } else {
            $picture = "";
        }
        $code = "<div class='imagegal' style='width:{$width}px;margin:{$this->settings['v_spacing']}px {$this->settings['h_spacing']}px;float:left;'>{$picture}" . $code . "</div>\n";
        //return "<a href='{$file}' rel='prettyPhoto[pp_gal]' title='You can add caption to pictures.'><img src='{$file}' width='60' height='60' alt='Red round shape' /></a>";
        return $code;
    }

    public function createImageGallery($files, $settings=false) {
        if ($settings === false) {
            $this->settings = $this->default_settings;
        } else {
            $this->settings = array_merge($this->default_settings, $settings);
        }
        $this->settings['actual_width'] = $this->settings['width'] - $this->settings['container_h_padding'] * 2;
        $this->settings['actual_height'] = $this->settings['height'] - $this->settings['container_v_padding'] * 2;
		foreach($files as $file) {
			$this->files[] = $file['name'];
		}
        $this->file_details = $files;

        $this->num_pages = $this->getNumberOfPages();
    }

    function calcThumbnailWidth() {
        $width = $this->settings['width'];
        $num_cols = $this->settings['col_limit'];
        $unit_width = (($width - $this->settings['container_h_padding'] * 2) / $num_cols) - 2 * $this->settings['h_spacing'];
        return $unit_width;
    }

    function calcThumbnailHeight($file) {
        //echo $_SERVER['PHP_SELF'] . $this->settings['htmlRoot'] .$file;
        list($width, $height, $type, $attr) = @getimagesize($this->settings['htmlRoot'] . $file);
        if($width<1) $width=1;
		if($height<1) $height=1;
		return $this->calcThumbnailWidth() / $width * $height;
		
    }

    function helperCreateMarginString() {
        return "margin:{$this->settings['v_spacing']}px {$this->settings['h_spacing']}px;";
    }

    function calcPageSize() {
        foreach ($this->files as $file) {
            $max_height = ($max_height > $this->calcThumbnailHeight($file) + $this->settings['v_spacing']) ? $max_height : $this->calcThumbnailHeight($file);
            if ($max_height > $this->settings['max_image_height']) {
                $max_height = $this->settings['max_image_height'];
            }
            if ($accHeight + $max_height > $this->settings['height'] - $this->settings['extra_space_h']) {
                break;
            }

            $code.=$this->getThumbnailCode($file, $this->calcThumbnailWidth());
            $colCount++;
            if ($colCount == $this->settings['col_limit']) {
                $accHeight+= $max_height;
                $max_height = 0;
                $colCount = 0;
            }
            $pageSize++;
        }
        return @$pageSize;
    }

    public function getNumberOfPages() {
        $this->page_mapping[1] = 0;
        $num_pages = 1;
        for ($i = 0; $i < sizeof($this->files); $i++) {
            $file = $this->files[$i];
            $max_height = ($max_height > $this->calcThumbnailHeight($file)) ? $max_height : $this->calcThumbnailHeight($file);
            if ($max_height > $this->settings['max_image_height']) {
                $max_height = $this->settings['max_image_height'];
            }
            if ($accHeight + $max_height > $this->settings['height'] - $this->settings['extra_space_h']) {
                $num_pages++;
                $accHeight = 0;
                $this->page_mapping[$num_pages] = $i;
            }

            $code.=$this->getThumbnailCode($file, $this->calcThumbnailWidth());
            $colCount++;
            if ($colCount == $this->settings['col_limit']) {
                $accHeight+= $max_height;
                $max_height = 0;
                $colCount = 0;
            }
        }
        return $num_pages;
    }

    public function createPaginationCode() {

        //$num_pages = ceil(sizeof($this->files) / $this->calcPageSize());
        $num_pages = $this->getNumberOfPages();
        $str .= "<div class='pagination'>";
        for ($i = 1; $i <= $num_pages; $i++) {
            if ($this->page_number == $i) {
                $str .= "<span class='current_page pages'>" . $i . "</span>";
            } else {
                $str .= "<a class='action_button' function='showGallery' pagenumber='{$i}' buttonid='{$this->settings['gallery_id']}'><span class='pages'>" . $i . "</span></a>";
            }
        }

        $str.="</div>";
        return $str;
    }

}

/*
  $files = array(
  "../images/menus/1.jpg",
  "../images/menus/2.jpg",
  "../images/menus/3.jpg",
  "../images/menus/4.jpg",
  "../images/menus/5.jpg",
  "../images/menus/6.jpg",
  "../images/menus/7.jpg",
  "../images/menus/8.jpg",
  "../images/menus/9.jpg",
  "../images/menus/10.jpg"
  );
  $imageGallery = new ImageGallery($files);
  $code = $imageGallery->setCode();
  echo $code;
 */
?>
