<!DOCTYPE HTML>

<html>

<head>
  <?php require_once('components/head.html'); ?>
  <title>Firkin Pubs | Restaurant | Bar | Restaurants | Best Restaurant</title>
  <meta name="title" content="Firkin Pubs | Restaurant | Bar | Restaurants | Best Restaurant" />
  <meta name="description" content="Careers at The Firkin Group of Pubs | Best British Bar-Style Restaurant | Authentic English fare and ale | Best Patios, Burgers &amp; Brunch" />
</head>

<body>
  <div class="wrapper">
    <?php require_once('components/header.html'); ?>
    <?php require_once('components/slider.html'); ?>
    <div class="content-wrapper with-banner" id="aboutUs">
      <div class="content">
        <h1 class="blue-heading">Join Us at <span>Firkin</span> Pubs, Bloke</h1>

        <p>If you've never been to a Firkin, why the Firkin not? See, we're not just a bar and we're not just a restaurant. We're an English style pub that serves some of the best Firkin fare in Canada. We do things a little differently around here. All
          of our dishes and drinks come with a Firkin twist&mdash;except our pints, we leave perfection alone. And all of our restaurant-style pubs are unique. But you'll fit in, everybody does at a Firkin. We're laid back with a wicked sense of humour.
          Everything a British bar should be.</p>

        <p>We are always looking for enthusiastic experienced candidates who are passionate about customer service</p>

        <h3> Now hiring for Assistant Managers</h3>

        <p>To apply, click the apply button below, and email your cover letter and resume to: careers@firkinpubs.com</p>

        <a href="/pdf/AM_Job_Description_August_2017.pdf">
          <div class="site-btn">
            Role Description
          </div>
        </a>
        <a href="mailto:careers@firkinpubs.com">
          <div class="site-btn">
            Apply Today!
          </div>
        </a>

      </div>
      <div class="clear"></div>
    </div>
    <?php require_once('components/footer.html'); ?>
  </div>
</body>
<?php require_once('components/scripts.html'); ?>
<script>
  $('#careersBtn').addClass('selected');
  $('#resCareersBtn').addClass('selected');
</script>

</html>
